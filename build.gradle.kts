import io.gitlab.jfronny.scripts.*

plugins {
    id("jf.autoversion")
}

allprojects {
    version = rootProject.version
    group = "io.gitlab.jfronny.inceptum"
}

val flavorProp: String by extra(prop("flavor", "custom"))
if (!setOf("custom", "maven", "fat", "windows", "linux", "macos").contains(flavorProp)) throw IllegalStateException("Unsupported flavor: $flavorProp")
val flavor: String by extra(if (flavorProp != "custom") flavorProp else OS.TYPE.codename)
val isPublic by extra(project.hasProperty("public"))
val isRelease by extra(project.hasProperty("release"))

val buildTime by extra(System.currentTimeMillis())
val wrapperVersion by extra(1)

val lwjglVersion = libs.versions.lwjgl.get()
val imguiVersion = libs.versions.imgui.get()
tasks.register("exportMetadata") {
    doLast {
        projectDir.resolve("version.json").writeText(
            """
            {
              "wrapperVersion": $wrapperVersion,
              "version": "$version",
              "buildTime": $buildTime,
              "isPublic": $isPublic,
              "isRelease": $isRelease,
              "jvm": ${project(":common").extra["javaVersion"]},
              "repositories": [
                "https://repo.maven.apache.org/maven2/",
                "https://maven.frohnmeyer-wds.de/artifacts/"
              ],
              "natives": {
                "windows": [
                  "org.lwjgl:lwjgl:$lwjglVersion:natives-windows",
                  "org.lwjgl:lwjgl-opengl:$lwjglVersion:natives-windows",
                  "org.lwjgl:lwjgl-glfw:$lwjglVersion:natives-windows",
                  "org.lwjgl:lwjgl-tinyfd:$lwjglVersion:natives-windows",
                  "io.github.spair:imgui-java-natives-windows:$imguiVersion"
                ],
                "linux": [
                  "org.lwjgl:lwjgl:$lwjglVersion:natives-linux",
                  "org.lwjgl:lwjgl-opengl:$lwjglVersion:natives-linux",
                  "org.lwjgl:lwjgl-glfw:$lwjglVersion:natives-linux",
                  "org.lwjgl:lwjgl-tinyfd:$lwjglVersion:natives-linux",
                  "io.github.spair:imgui-java-natives-linux:$imguiVersion"
                ],
                "macos": [
                  "org.lwjgl:lwjgl:$lwjglVersion:natives-macos",
                  "org.lwjgl:lwjgl-opengl:$lwjglVersion:natives-macos",
                  "org.lwjgl:lwjgl-glfw:$lwjglVersion:natives-macos",
                  "org.lwjgl:lwjgl-tinyfd:$lwjglVersion:natives-macos",
                  "io.github.spair:imgui-java-natives-macos:$imguiVersion"
                ]
              }
            }
            """.trimIndent()
        )
    }
}