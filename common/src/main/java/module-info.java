module io.gitlab.jfronny.inceptum.common {
    exports io.gitlab.jfronny.inceptum.common;
    exports io.gitlab.jfronny.inceptum.common.api;
    exports io.gitlab.jfronny.inceptum.common.model.inceptum;
    exports io.gitlab.jfronny.inceptum.common.model.maven;

    requires transitive java.desktop;
    requires java.xml;
    requires transitive io.gitlab.jfronny.commons;
    requires transitive io.gitlab.jfronny.commons.http.client;
    requires transitive io.gitlab.jfronny.commons.io;
    requires transitive io.gitlab.jfronny.commons.logger;
    requires transitive io.gitlab.jfronny.commons.serialize.json;
    requires static org.jetbrains.annotations;
    requires static io.gitlab.jfronny.commons.serialize.generator.annotations;
    requires io.gitlab.jfronny.commons.serialize;
}