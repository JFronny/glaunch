package io.gitlab.jfronny.inceptum.common.model.inceptum;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;

import java.util.Map;
import java.util.Set;

@GSerializable
public record UpdateMetadata(int wrapperVersion,
                             String version,
                             long buildTime,
                             boolean isPublic,
                             boolean isRelease,
                             int jvm,
                             Set<String> repositories,
                             Map<String, Set<String>> natives) {
}
