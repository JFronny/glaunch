package io.gitlab.jfronny.inceptum.common;

import io.gitlab.jfronny.commons.http.client.HttpClient;
import io.gitlab.jfronny.commons.io.HashUtils;
import io.gitlab.jfronny.commons.serialize.json.JsonReader;
import io.gitlab.jfronny.commons.serialize.json.JsonTransport;
import io.gitlab.jfronny.commons.throwable.ThrowingBiFunction;
import io.gitlab.jfronny.commons.throwable.ThrowingFunction;
import io.gitlab.jfronny.commons.throwable.ThrowingSupplier;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

public class Net {
    private static final ObjectCache OBJECT_CACHE = new ObjectCache(MetaHolder.CACHE_DIR);

    public static byte[] downloadData(String url) throws IOException, URISyntaxException {
        try (InputStream is = HttpClient.get(url).sendInputStream()) {
            return is.readAllBytes();
        }
    }

    public static byte[] downloadData(String url, String sha1) throws IOException, URISyntaxException {
        byte[] buf = downloadData(url);
        if (sha1 == null) return buf;
        if (!HashUtils.sha1(buf).equals(sha1)) throw new IOException("Invalid hash");
        return buf;
    }

    public static <T> T downloadObject(String url, ThrowingFunction<String, T, IOException> func) throws IOException {
        return downloadObject(url, func, true);
    }

    public static <T> T downloadJObject(String url, ThrowingFunction<JsonReader, T, IOException> func) throws IOException {
        return downloadJObject(url, func, true);
    }

    public static <T> T downloadObject(String url, ThrowingFunction<String, T, IOException> func, boolean cache) throws IOException {
        return downloadObject(url, () -> HttpClient.get(url).sendString(), func, cache);
    }

    public static <T> T downloadJObject(String url, ThrowingFunction<JsonReader, T, IOException> func, boolean cache) throws IOException {
        return downloadJObject(url, () -> HttpClient.get(url).sendString(), func, cache);
    }

    public static <T> T downloadObject(String url, ThrowingFunction<String, T, IOException> func, String apiKey) throws IOException {
        return downloadObject(url, () -> downloadStringAuthenticated(url, apiKey), func, true);
    }

    public static <T> T downloadJObject(String url, ThrowingFunction<JsonReader, T, IOException> func, String apiKey) throws IOException {
        return downloadJObject(url, () -> downloadStringAuthenticated(url, apiKey), func, true);
    }

    public static <T> T downloadObject(String url, String sha1, ThrowingFunction<String, T, IOException> func) throws IOException {
        return downloadObject(url, sha1, func, true);
    }

    public static <T> T downloadJObject(String url, String sha1, ThrowingFunction<JsonReader, T, IOException> func) throws IOException {
        return downloadJObject(url, sha1, func, true);
    }

    public static <T> T downloadObject(String url, String sha1, ThrowingFunction<String, T, IOException> func, boolean cache) throws IOException {
        return downloadObject(url, () -> downloadString(url, sha1), func, cache);
    }

    public static <T> T downloadJObject(String url, String sha1, ThrowingFunction<JsonReader, T, IOException> func, boolean cache) throws IOException {
        return downloadJObject(url, () -> downloadString(url, sha1), func, cache);
    }

    private static <T> T downloadObject(String url, ThrowingSupplier<String, Exception> sourceString, ThrowingFunction<String, T, IOException> func, boolean cache) throws IOException {
        try {
            ThrowingSupplier<T, Exception> builder = () -> func.apply(sourceString.get());
            return cache ? OBJECT_CACHE.get(HashUtils.sha1(url.getBytes(StandardCharsets.UTF_8)), sourceString, func) : builder.get();
        } catch (Exception e) {
            throw new IOException("Could not download object and no cache exists", e);
        }
    }

    public static <T> T downloadJObject(String url, ThrowingSupplier<String, Exception> sourceString, ThrowingFunction<JsonReader, T, IOException> func, boolean cache) throws IOException {
        return downloadObject(url, sourceString, s -> func.apply(GsonPreset.API.createReader(s)), cache);
    }

    public static String buildUrl(String host, String url, Map<String, String> params) {
        StringBuilder res = new StringBuilder(host);
        if (res.toString().endsWith("/")) res = new StringBuilder(res.substring(0, res.length() - 1));
        if (url.startsWith("/")) res.append(url);
        else res.append("/").append(url);
        int i = 0;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            res.append(i++ == 0 ? '?' : '&')
                    .append(URLEncoder.encode(entry.getKey(), StandardCharsets.UTF_8))
                    .append('=')
                    .append(URLEncoder.encode(entry.getValue(), StandardCharsets.UTF_8));
        }
        return res.toString();
    }

    public static String downloadString(String url) throws IOException, URISyntaxException {
        return HttpClient.get(url).sendString();
    }

    public static String downloadString(String url, String sha1) throws IOException, URISyntaxException {
        return new String(downloadData(url, sha1), StandardCharsets.UTF_8);
    }

    public static String downloadStringAuthenticated(String url, String apiKey) throws IOException, URISyntaxException {
        return HttpClient.get(url).header("x-api-key", apiKey).sendString();
    }

    public static void downloadFile(String url, Path path) throws IOException, URISyntaxException {
        if (!Files.exists(path.getParent())) Files.createDirectories(path.getParent());
        Files.write(path, downloadData(url));
    }

    public static void downloadFile(String url, String sha1, Path path) throws IOException, URISyntaxException {
        if (!Files.exists(path.getParent())) Files.createDirectories(path.getParent());
        Files.write(path, downloadData(url, sha1));
    }
}
