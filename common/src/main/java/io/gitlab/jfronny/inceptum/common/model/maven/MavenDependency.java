package io.gitlab.jfronny.inceptum.common.model.maven;

public record MavenDependency(String groupId, String artifactId, String version, String scope) {
}
