package io.gitlab.jfronny.inceptum.common;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GComment;
import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.model.inceptum.UpdateChannel;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@GSerializable(isStatic = true)
public class InceptumConfig {
    @GComment("Whether to show snapshots in the version selector for new instances")
    public static boolean snapshots = false;
    @GComment("Whether to launch the ImGUI in dark mode\nConfigurable in Settings->Dark Theme")
    public static boolean darkTheme = false;
    @GComment("Whether the GTK UI should default to a list view instead of a grid")
    public static boolean listView = false;
    @GComment("Whether to require an account to launch the game\nIntended to allow running the game from USB sticks on constrained networks")
    public static boolean enforceAccount = true;
    @GComment("The currently selected account\nUsed to launch the game")
    public static String lastAccount = null;
    @GComment("The last name used for an offline session")
    public static String offlineAccountLastName = null;
    @GComment("The update channel. Either \"CI\" or \"Stable\"\nI personally recommend the CI channel as it gets the latest fixes and features quicker")
    public static UpdateChannel channel = UpdateChannel.Stable;
    @GComment("The author name to add to packs where the metadata format requires specifying one")
    public static String authorName = "Inceptum";

    public static void load() throws IOException {
        if (!Files.exists(MetaHolder.CONFIG_PATH.getParent()))
            Files.createDirectories(MetaHolder.CONFIG_PATH.getParent());
        if (!Files.exists(MetaHolder.CONFIG_PATH)) {
            Path gLaunch2 = MetaHolder.BASE_PATH.resolve("glaunch2.json");
            Path json = MetaHolder.BASE_PATH.resolve("inceptum.json");
            if (Files.exists(gLaunch2)) {
                Files.move(gLaunch2, MetaHolder.CONFIG_PATH);
            } else if (Files.exists(json)) {
                Files.move(json, MetaHolder.CONFIG_PATH);
            } else {
                saveConfig();
            }
        }
        GC_InceptumConfig.deserialize(MetaHolder.CONFIG_PATH, GsonPreset.CONFIG);
    }

    public static void saveConfig() {
        try {
            GC_InceptumConfig.serialize(MetaHolder.CONFIG_PATH, GsonPreset.CONFIG);
        } catch (IOException e) {
            Utils.LOGGER.error("Could not save config", e);
        }
    }
}
