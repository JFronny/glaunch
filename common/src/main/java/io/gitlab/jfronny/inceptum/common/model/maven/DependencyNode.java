package io.gitlab.jfronny.inceptum.common.model.maven;

import java.util.Iterator;
import java.util.Set;

public record DependencyNode(String name, Set<DependencyNode> dependencies) {
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        generateTree(sb, "", "");
        return sb.toString();
    }

    private void generateTree(StringBuilder sb, String prefix, String childrenPrefix) {
        sb.append(prefix).append(name).append('\n');
        for (Iterator<DependencyNode> it = dependencies.iterator(); it.hasNext(); ) {
            DependencyNode next = it.next();
            if (it.hasNext()) {
                next.generateTree(sb, childrenPrefix + "├── ", childrenPrefix + "│   ");
            } else {
                next.generateTree(sb, childrenPrefix + "└── ", childrenPrefix + "    ");
            }
        }
    }
}
