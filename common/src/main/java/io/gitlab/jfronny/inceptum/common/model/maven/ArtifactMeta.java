package io.gitlab.jfronny.inceptum.common.model.maven;

import io.gitlab.jfronny.inceptum.common.MetaHolder;
import org.jetbrains.annotations.Nullable;

import java.nio.file.Path;
import java.util.Objects;

public record ArtifactMeta(String groupId, String artifactId, String version, @Nullable String classifier, @Nullable String snapshotVersion) {
    public ArtifactMeta(String groupId, String artifactId, String version) {
        this(groupId, artifactId, version, null, null);
    }

    public static ArtifactMeta parse(String mavenNotation) {
        if (Objects.requireNonNull(mavenNotation).isEmpty()) throw new IllegalArgumentException("The notation is empty");
        String[] lib = mavenNotation.split(":");
        if (lib.length <= 1) throw new IllegalArgumentException("Not in maven notation");
        if (lib.length == 2) throw new IllegalArgumentException("Skipping versions is not supported");
        if (lib.length >= 5) throw new IllegalArgumentException("Unkown elements in maven notation");
        return new ArtifactMeta(lib[0], lib[1], lib[2], lib.length > 3 ? lib[3] : null, null);
    }

    public String getPomPath() {
        String path = groupId.replace('.', '/') + '/';
        path += artifactId + '/';
        path += version + '/';
        path += artifactId + '-';
        if (snapshotVersion != null) path += version.replace("SNAPSHOT", snapshotVersion);
        else path += version;
        return path + ".pom";
    }

    public String getJarPath(boolean respectSnapshotVersion) {
        String path = groupId.replace('.', '/') + '/';
        path += artifactId + '/';
        path += version + '/';
        path += artifactId + '-';
        if (snapshotVersion != null && respectSnapshotVersion) path += version.replace("SNAPSHOT", snapshotVersion);
        else path += version;
        if (classifier != null) path += '-' + classifier;
        return path + ".jar";
    }

    public String getMavenNotation() {
        String notation = groupId + ':' + artifactId + ':' + version;
        if (classifier != null) notation += ':' + classifier;
        return notation;
    }

    public String getMetadataPath() {
        return groupId.replace('.', '/') + '/' + artifactId + '/' + version + "/maven-metadata.xml";
    }

    public Path getLocalPath() {
        return MetaHolder.LIBRARIES_DIR.resolve(getJarPath(false));
    }
}
