import io.gitlab.jfronny.scripts.*
import javax.lang.model.element.Modifier

plugins {
    inceptum.library
    inceptum.`gson-compile`
}

dependencies {
    api(libs.bundles.commons)
}

val javaVersion by extra(project.java.targetCompatibility)

sourceSets {
    main {
        generate(project) {
            `class`("io.gitlab.jfronny.inceptum.common", "BuildMetadata") {
                modifiers(Modifier.PUBLIC)
                val modifiers = arrayOf(Modifier.PUBLIC, Modifier.STATIC, Modifier.FINAL)

                field("VERSION", versionS, *modifiers)
                field("BUILD_TIME", rootProject.extra["buildTime"] as Long, *modifiers)
                field("IS_PUBLIC", rootProject.extra["isPublic"] as Boolean, *modifiers)
                field("IS_RELEASE", rootProject.extra["isRelease"] as Boolean, *modifiers)
                field("VM_VERSION", javaVersion.majorVersion.toInt(), *modifiers)
                field("WRAPPER_VERSION", rootProject.extra["wrapperVersion"] as Int, *modifiers)
            }
        }
    }
}
