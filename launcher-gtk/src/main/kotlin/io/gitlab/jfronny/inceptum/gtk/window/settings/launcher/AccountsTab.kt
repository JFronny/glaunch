package io.gitlab.jfronny.inceptum.gtk.window.settings.launcher

import io.gitlab.jfronny.inceptum.gtk.GtkMenubar
import io.gitlab.jfronny.inceptum.gtk.control.ILabel
import io.gitlab.jfronny.inceptum.gtk.control.settings.SectionedSettingsTab
import io.gitlab.jfronny.inceptum.gtk.util.margin
import io.gitlab.jfronny.inceptum.gtk.window.dialog.MicrosoftLoginDialog
import io.gitlab.jfronny.inceptum.launcher.api.account.AccountManager
import org.gnome.gtk.*

class AccountsTab(window: Window?) : SectionedSettingsTab<Window>(window) {
    init {
        section(null) {
            build()
        }
    }

    private fun Section.build() {
        generateRows()
        val row = Button.fromIconName("list-add-symbolic")
        row(row)
        row.onClicked {
            MicrosoftLoginDialog(window) {
                clear()
                build()
                GtkMenubar.generateAccountsMenu(window!!.application!!)
            }.visible = true
        }
    }

    private fun Section.generateRows() {
        for (account in AccountManager.getAccounts()) {
            val row = Box(Orientation.HORIZONTAL, 40)
            val ref = row(row)
            row.margin = 8
            //TODO profile icon
            val head = Box(Orientation.VERTICAL, 0)
            head.hexpand = true
            head.halign = Align.START
            head.valign = Align.CENTER
            val title = Label(account.minecraftUsername)
            title.halign = Align.START
            head.append(title)
            val subtitle = Label(account.uuid)
            ILabel.theme(subtitle, ILabel.Mode.SUBTITLE)
            subtitle.halign = Align.START
            head.append(subtitle)
            row.append(head)
            val remove = Button.fromIconName("window-close-symbolic")
            remove.valign = Align.CENTER
            remove.halign = Align.END
            remove.onClicked {
                AccountManager.removeAccount(account)
                remove(ref!!)
                GtkMenubar.generateAccountsMenu(window!!.application!!)
            }
            row.append(remove)
        }
    }
}
