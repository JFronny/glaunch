package io.gitlab.jfronny.inceptum.gtk.window

import io.gitlab.jfronny.inceptum.common.BuildMetadata
import io.gitlab.jfronny.inceptum.gtk.util.I18n
import org.gnome.gtk.AboutDialog
import org.gnome.gtk.License

class AboutWindow : AboutDialog() {
    init {
        programName = "Inceptum"
        copyright = "Copyright (C) 2021-2023 JFronny"
        version = BuildMetadata.VERSION
        licenseType = License.MIT_X11
        license = I18n["about.license"]
        websiteLabel = I18n["about.contact"]
        website = "https://jfronny.gitlab.io/contact.html"
        if (!BuildMetadata.IS_PUBLIC) {
            comments = I18n["about.unsupported-build"]
        }
        val vm = Runtime.version().feature()
        systemInformation = I18n[if (BuildMetadata.VM_VERSION == vm) "about.jvm" else "about.jvm.unsupported", vm]
        //TODO setLogo
    }

    companion object {
        fun createAndShow() {
            AboutWindow().visible = true
        }
    }
}
