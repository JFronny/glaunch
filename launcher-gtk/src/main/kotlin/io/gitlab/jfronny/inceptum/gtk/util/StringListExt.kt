package io.gitlab.jfronny.inceptum.gtk.util

import org.gnome.gtk.StringList

fun StringList.addAll(values: Array<String>) = splice(size, 0, values)
fun StringList.replaceAll(values: Array<String>) = splice(0, size, values)