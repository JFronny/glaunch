package io.gitlab.jfronny.inceptum.gtk.control.settings

import io.gitlab.jfronny.commons.StringFormatter
import io.gitlab.jfronny.inceptum.gtk.GtkEnvBackend
import org.gnome.gtk.Widget
import org.gnome.gtk.Window

open class SettingsTab<T : Widget, W : Window>(
    protected val window: W?,
    val content: T,
    val scrollable: Boolean
) {
    protected fun showError(message: String, t: Throwable) =
        GtkEnvBackend.simpleDialog(
            window,
            StringFormatter.toString(t),
            message,
            null,
            null
        )
}
