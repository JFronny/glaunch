package io.gitlab.jfronny.inceptum.gtk.util

import io.gitlab.jfronny.commons.logger.SystemLoggerPlus
import io.gitlab.jfronny.inceptum.common.Utils
import org.gnome.glib.LogField
import org.gnome.glib.LogLevelFlags
import org.gnome.glib.LogWriterFunc
import org.gnome.glib.LogWriterOutput

object Log : SystemLoggerPlus by Utils.LOGGER
object GtkLog : LogWriterFunc {
    // Based on https://github.com/jwharm/java-gi-examples/blob/6e08b4d2e62998c53d855b32433513308e01e82d/Logging/src/main/java/io/github/jwharm/javagi/examples/logging/SLF4JLogWriterFunc.java
    // Original license: LGPL-2.1, written by Jan-Willem Harmannij
    override fun run(logLevel: Set<LogLevelFlags>, fields: Array<LogField>): LogWriterOutput {
        val level = logLevel.asSequence()
            .map { when (it) {
                LogLevelFlags.LEVEL_ERROR, LogLevelFlags.LEVEL_CRITICAL -> System.Logger.Level.ERROR
                LogLevelFlags.LEVEL_WARNING -> System.Logger.Level.WARNING
                LogLevelFlags.LEVEL_MESSAGE, LogLevelFlags.LEVEL_INFO -> System.Logger.Level.INFO
                LogLevelFlags.LEVEL_DEBUG -> System.Logger.Level.DEBUG
                else -> null
            } }
            .filterNotNull()
            .maxByOrNull(System.Logger.Level::ordinal)
            ?: return LogWriterOutput.UNHANDLED
        var domain: String? = null
        var file: String? = null
        var func: String? = null
        var line: String? = null
        var message: String? = null
        for (field in fields) {
            when (field.readKey()) {
                "GLIB_DOMAIN" -> domain = field.readString()
                "CODE_FILE" -> file = field.readString()
                "CODE_FUNC" -> func = "::${field.readString()}"
                "CODE_LINE" -> line = ":${field.readString()}"
                "MESSAGE" -> message = field.readString()
            }
        }
        if (domain == null || message == null) return LogWriterOutput.UNHANDLED
        System.getLogger(domain).log(level, "{0}{1}{2}: {3}", file ?: "<unknown>", line ?: "", func ?: "", message)
        return LogWriterOutput.HANDLED
    }

    private fun LogField.readString(): String {
        val length = readLength()
        return readValue().reinterpret(if (length == -1L) Long.MAX_VALUE else length).getString(0)
    }
}