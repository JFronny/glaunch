package io.gitlab.jfronny.inceptum.gtk.window.settings.instance

import io.gitlab.jfronny.inceptum.gtk.control.settings.SettingsWindow
import io.gitlab.jfronny.inceptum.launcher.system.instance.Instance
import org.gnome.gtk.Application

class InstanceSettingsWindow(val app: Application?, val instance: Instance) : SettingsWindow(app) {
    init {
        val claim = instance.mds.focus()
        instance.mds.start()
        addTab(GeneralTab(this), "instance.settings.general", "preferences-other-symbolic")
        addTab(ModsTab(this), "instance.settings.mods", "package-x-generic-symbolic")
        addTab(ExportTab(this), "instance.settings.export", "send-to-symbolic")
        onCloseRequest {
            claim.close()
            false
        }
    }
}
