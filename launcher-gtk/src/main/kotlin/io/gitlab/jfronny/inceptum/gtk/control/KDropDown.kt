package io.gitlab.jfronny.inceptum.gtk.control

import io.gitlab.jfronny.inceptum.gtk.util.I18n
import org.gnome.gtk.DropDown
import org.gnome.gtk.PropertyExpression
import org.gnome.gtk.StringList
import org.gnome.gtk.StringObject
import org.jetbrains.annotations.PropertyKey
import java.util.function.IntConsumer

class KDropDown<T>(options: Array<T>, private val stringify: (T) -> String, selected: Int): DropDown(options.toModel(stringify), null) {
    private val onChange = ArrayList<IntConsumer>()

    init {
        this.selected = selected
        onNotify("selected") { _ -> onChange.forEach { it.accept(this.selected) } }
        expression = PropertyExpression(StringObject.getType(), null, "string")
    }

    fun onChange(changed: IntConsumer) {
        onChange.add(changed)
    }

    fun updateOptions(newOptions: Array<T>, newSelected: Int) {
        selected = 0
        model = newOptions.toModel(stringify)
        selected = newSelected
    }

    companion object {
        private fun <T> Array<T>.toModel(stringify: (T) -> String) = StringList(map(stringify).toTypedArray())
    }
}

fun KDropDown(vararg options: @PropertyKey(resourceBundle = I18n.BUNDLE) String, selected: Int = 0): KDropDown<String> =
    KDropDown(options.map { it }.toTypedArray(), { I18n[it] }, selected)