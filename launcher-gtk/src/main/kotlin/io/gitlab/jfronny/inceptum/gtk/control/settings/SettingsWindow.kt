package io.gitlab.jfronny.inceptum.gtk.control.settings

import io.gitlab.jfronny.inceptum.gtk.util.I18n
import org.gnome.adw.HeaderBar
import org.gnome.adw.ViewStack
import org.gnome.adw.ViewSwitcherBar
import org.gnome.adw.ViewSwitcherTitle
import org.gnome.gobject.BindingFlags
import org.gnome.gtk.*
import org.jetbrains.annotations.PropertyKey

open class SettingsWindow(app: Application?) : Window() {
    protected val stack: ViewStack

    init {
        application = app

        stack = ViewStack().apply {
            vexpand = true
        }

        val header = HeaderBar()
        val viewSwitcher = ViewSwitcherTitle()
        viewSwitcher.stack = stack
        header.titleWidget = viewSwitcher
        titlebar = header

        val bottomBar = ViewSwitcherBar()
        bottomBar.stack = stack
        viewSwitcher.bindProperty("title-visible", bottomBar, "reveal", BindingFlags.DEFAULT)
        val view = Box(Orientation.VERTICAL, 0)
        view.append(stack)
        view.append(bottomBar)

        child = view

        setDefaultSize(720, 360)
    }

    fun addTab(tab: SettingsTab<*, *>, title: @PropertyKey(resourceBundle = I18n.BUNDLE) String, iconName: String) {
        if (tab.scrollable) {
            stack.addTitledWithIcon(ScrolledWindow().apply {
                child = tab.content
            }, title, I18n[title], iconName)
        } else {
            stack.addTitledWithIcon(tab.content, title, I18n[title], iconName)
        }
    }

    var activePage: String
        get() = stack.visibleChildName!!
        set(@PropertyKey(resourceBundle = I18n.BUNDLE) title) { stack.visibleChildName = title }
}
