package io.gitlab.jfronny.inceptum.gtk.window.settings.launcher

import io.gitlab.jfronny.inceptum.common.InceptumConfig
import io.gitlab.jfronny.inceptum.common.model.inceptum.UpdateChannel
import io.gitlab.jfronny.inceptum.gtk.control.settings.SectionedSettingsTab
import org.gnome.gtk.Window

class GeneralTab(window: Window?) : SectionedSettingsTab<Window>(window) {
    init {
        section(null) {
            row("settings.general.snapshots", "settings.general.snapshots.subtitle") {
                setSwitch(InceptumConfig.snapshots) { b: Boolean? ->
                    InceptumConfig.snapshots = b!!
                    InceptumConfig.saveConfig()
                }
            }
            row("settings.general.update-channel", "settings.general.update-channel.subtitle") {
                setDropdown(arrayOf("Stable", "CI"), if (InceptumConfig.channel == UpdateChannel.CI) 1 else 0) { state ->
                    InceptumConfig.channel = if (state == 1) UpdateChannel.CI else UpdateChannel.Stable
                    InceptumConfig.saveConfig()
                }
            }
            row("settings.general.author-name", "settings.general.author-name.subtitle") {
                setEntry(InceptumConfig.authorName) { s ->
                    InceptumConfig.authorName = s
                    InceptumConfig.saveConfig()
                }
            }
        }
    }
}
