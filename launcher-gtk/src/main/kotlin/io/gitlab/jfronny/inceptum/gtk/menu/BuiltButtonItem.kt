package io.gitlab.jfronny.inceptum.gtk.menu

import org.gnome.gio.MenuItem
import org.gnome.gio.SimpleAction

class BuiltButtonItem(action: SimpleAction, menuItem: MenuItem?) : BuiltMenuItem(action, menuItem)
