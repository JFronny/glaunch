package io.gitlab.jfronny.inceptum.gtk.window.settings.launcher

import io.gitlab.jfronny.inceptum.gtk.control.settings.SettingsWindow
import org.gnome.gtk.Application

class LauncherSettingsWindow(app: Application) : SettingsWindow(app) {
    init {
        addTab(GeneralTab(this), "settings.general", "preferences-other-symbolic")
        addTab(AccountsTab(this), "settings.accounts", "system-users-symbolic")
    }
}
