package io.gitlab.jfronny.inceptum.gtk.window.dialog

import org.gnome.gtk.*

class StringInputDialog(parent: Window?, flags: Set<DialogFlags>, type: MessageType, buttons: ButtonsType, message: String, value: String) : MessageDialog(parent, flags, type, buttons, message) {
    private val entry = Entry()

    init {
        (messageArea as Box).append(entry)
        entry.text = value
    }

    val input: String get() = entry.text
}
