plugins {
    inceptum.`application-standalone`
}

application {
    mainClass.set("io.gitlab.jfronny.inceptum.wrapper.Wrapper")
}

dependencies {
    implementation(projects.common)
}

tasks.runShadow.get().workingDir = rootProject.projectDir