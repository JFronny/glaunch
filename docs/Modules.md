# Modules
Inceptum is split into multiple separate but interdependent modules.
The purpose of this page is to list them, explain their purpose and where to get them

## common
This module contains common, platform-agnostic code shared between the launcher and the wrapper.
It can be obtained through the maven.

## launcher
This module contains common, platform-agnostic code between all frontends of the launcher.
It can be obtained through the maven.

## launcher-cli
This module contains the platform-agnostic command-line interface for the launcher
It can be obtained through the maven or the shadowed Inceptum jar

## launcher-imgui
This module contains a dear-imgui-based frontend for Inceptum.
Builds of this module are platform-specific and dependents must manually ensure the correct imgui and lwjgl natives are imported.
A build without natives can be obtained through the maven and a build with natives through the shadowed Inceptum jar

## launcher-dist/Inceptum
This module builds a shadowed jar of launcher-cli and launcher-imgui to be used by normal users.
It also adds additional, platform-specific commands to the CLI.
A shadowed build can be obtained as "Inceptum" from maven, a build with dependencies as "launcher-dist"
Windows users can also obtain a binary built using fabric-installer-native-bootstrap.

## launchwrapper
This module is added to the minecraft classpath and therefore independent of any other modules.
It handles loading forceload natives

## wrapper
This module serves the purpose of downloading the components necessary for executing Inceptum on the current platform.
A build with shadowed dependencies can be obtained through the maven (with the suffix "all") or as a jar.
Windows users can also obtain a binary built using fabric-installer-native-bootstrap.
