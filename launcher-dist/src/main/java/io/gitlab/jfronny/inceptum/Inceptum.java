package io.gitlab.jfronny.inceptum;

import io.gitlab.jfronny.inceptum.cli.CliMain;

public class Inceptum {
    private static final GuiCommand GUI_COMMAND = new GuiCommand();

    public static void main(String[] args) throws Exception {
        CliMain.KNOWN_COMMANDS.add(1, GUI_COMMAND);
        CliMain.KNOWN_COMMANDS.add(new UpdateCheckCommand());
        CliMain.DEFAULT = GUI_COMMAND;
        CliMain.main(args);
    }
}