package io.gitlab.jfronny.inceptum;

import io.gitlab.jfronny.inceptum.cli.Command;
import io.gitlab.jfronny.inceptum.cli.CommandArgs;
import io.gitlab.jfronny.inceptum.imgui.GuiEnvBackend;
import io.gitlab.jfronny.inceptum.imgui.GuiMain;
import io.gitlab.jfronny.inceptum.launcher.LauncherEnv;

public class GuiCommand extends Command {
    public GuiCommand() {
        super("Displays the Inceptum UI", "", "gui", "show");
    }

    @Override
    public void invoke(CommandArgs args) {
        LauncherEnv.updateBackend(new GuiEnvBackend());
        GuiMain.showGui();
    }

    @Override
    public boolean enableLog() {
        return true;
    }
}
