plugins {
    id("inceptum.library")
}

val libs = extensions.getByType<VersionCatalogsExtension>().named("libs")
dependencies {
    compileOnly(libs.findLibrary("commons-serialize-generator-annotations").orElseThrow())
    annotationProcessor(libs.findLibrary("commons-serialize-generator").orElseThrow())
}

tasks.withType<JavaCompile> {
    options.compilerArgs.addAll(listOf("-AserializeProcessorNoReflect"))
}