import de.undercouch.gradle.tasks.download.Download
import java.io.FileOutputStream

plugins {
    application
    id("inceptum.java")
    com.gradleup.shadow
    de.undercouch.download
}

abstract class FileOutput : DefaultTask() {
    @get:OutputFile
    abstract var output: File
}

val bootstrapVersion = "0.3.2"
val bootstrapArch = "i686"

val downloadBootstrap by tasks.registering(Download::class) {
    src("https://maven.fabricmc.net/net/fabricmc/fabric-installer-native-bootstrap/windows-${bootstrapArch}/${bootstrapVersion}/windows-${bootstrapArch}-${bootstrapVersion}.exe")
    dest(project.layout.buildDirectory)
}

val nativeExe by tasks.registering(FileOutput::class) {
    dependsOn(downloadBootstrap)
    dependsOn(tasks.shadowJar)

    output = project.layout.buildDirectory.file("libs/${project.name}-${project.version}.exe").get().asFile
    outputs.upToDateWhen { false }

    doFirst {
        output.delete()
    }

    doLast {
        output.createNewFile()
        FileOutputStream(output).use { w ->
            w.write(downloadBootstrap.get().outputFiles.first().readBytes())
            w.write(tasks.shadowJar.get().archiveFile.get().asFile.readBytes())
        }
    }
}

if (rootProject.extra["flavor"] == "windows") {
    tasks.build.get().dependsOn(nativeExe)
}

tasks.runShadow {
    workingDir = rootProject.projectDir
}