plugins {
    application
    id("inceptum.java")
}

publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            from(components["java"])
        }
    }
}

tasks.run.get().workingDir = rootProject.projectDir