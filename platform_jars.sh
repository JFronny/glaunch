#!/bin/bash
set -xe

# shellcheck disable=SC2144
function moveartifacts {
  [ -f launcher-dist/build/jpackage/*.deb ] && mv launcher-dist/build/jpackage/*.deb public/inceptum.deb
  [ -f launcher-dist/build/jpackage/*.msi ] && mv launcher-dist/build/jpackage/*.msi public/inceptum.msi
  for f in launcher-dist/build/libs/Inceptum-*-*-*.jar; do mv "$f" "public/Inceptum-${f##*-}"; done
}

gradle --build-cache :launcher-dist:build -Pflavor=fat -Ppublic -Ptimestamp=${CI_PIPELINE_STARTED}
moveartifacts
mv public/Inceptum-fat.jar public/Inceptum.jar
gradle --build-cache :launcher-dist:build :launcher-dist:jpackage -Pflavor=windows -Ppublic -Pcrosscompile -Ptimestamp=${CI_PIPELINE_STARTED}
moveartifacts
gradle --build-cache :launcher-dist:build :launcher-dist:jpackage -Pflavor=linux -Ppublic -Ptimestamp=${CI_PIPELINE_STARTED}
moveartifacts
gradle --build-cache :launcher-dist:build -Pflavor=macos -Ppublic -Ptimestamp=${CI_PIPELINE_STARTED}
moveartifacts