module io.gitlab.jfronny.inceptum.launcher {
    exports io.gitlab.jfronny.inceptum.launcher;
    exports io.gitlab.jfronny.inceptum.launcher.api;
    exports io.gitlab.jfronny.inceptum.launcher.api.account;
    exports io.gitlab.jfronny.inceptum.launcher.gson;
    exports io.gitlab.jfronny.inceptum.launcher.model.curseforge;
    exports io.gitlab.jfronny.inceptum.launcher.model.curseforge.response;
    exports io.gitlab.jfronny.inceptum.launcher.model.fabric;
    exports io.gitlab.jfronny.inceptum.launcher.model.inceptum;
    exports io.gitlab.jfronny.inceptum.launcher.model.microsoft;
    exports io.gitlab.jfronny.inceptum.launcher.model.microsoft.request;
    exports io.gitlab.jfronny.inceptum.launcher.model.microsoft.response;
    exports io.gitlab.jfronny.inceptum.launcher.model.modrinth;
    exports io.gitlab.jfronny.inceptum.launcher.model.mojang;
    exports io.gitlab.jfronny.inceptum.launcher.model.multimc;
    exports io.gitlab.jfronny.inceptum.launcher.system.exporter;
    exports io.gitlab.jfronny.inceptum.launcher.system.importer;
    exports io.gitlab.jfronny.inceptum.launcher.system.instance;
    exports io.gitlab.jfronny.inceptum.launcher.system.launch;
    exports io.gitlab.jfronny.inceptum.launcher.system.mds;
    exports io.gitlab.jfronny.inceptum.launcher.system.mds.noop;
    exports io.gitlab.jfronny.inceptum.launcher.system.setup;
    exports io.gitlab.jfronny.inceptum.launcher.system.setup.steps;
    exports io.gitlab.jfronny.inceptum.launcher.system.source;
    exports io.gitlab.jfronny.inceptum.launcher.util;
    exports io.gitlab.jfronny.inceptum.launcher.util.gitignore;

    requires transitive io.gitlab.jfronny.inceptum.common;
    requires transitive io.gitlab.jfronny.commons.http.server;
    requires static org.jetbrains.annotations;
    requires static io.gitlab.jfronny.commons.serialize.generator.annotations;
    requires io.gitlab.jfronny.commons.serialize;
    requires io.gitlab.jfronny.commons.flow;
}