package io.gitlab.jfronny.inceptum.launcher.system.setup.steps;

import io.gitlab.jfronny.inceptum.common.GsonPreset;
import io.gitlab.jfronny.inceptum.common.MetaHolder;
import io.gitlab.jfronny.inceptum.launcher.model.inceptum.GC_InstanceMeta;
import io.gitlab.jfronny.inceptum.launcher.system.instance.Instance;
import io.gitlab.jfronny.inceptum.launcher.system.mds.ModsDirScanner;
import io.gitlab.jfronny.inceptum.launcher.system.mds.ScanStage;
import io.gitlab.jfronny.inceptum.launcher.system.setup.SetupStepInfo;
import io.gitlab.jfronny.inceptum.launcher.system.setup.Step;

import java.io.IOException;
import java.nio.file.Path;

public class RunMdsStep implements Step {
    @Override
    public void execute(SetupStepInfo info) throws IOException {
        info.setState("Running MDS");
        Path instance = MetaHolder.INSTANCE_DIR.resolve(info.name());
        var mds = ModsDirScanner.get(instance.resolve("mods"), GC_InstanceMeta.deserialize(instance.resolve(Instance.CONFIG_NAME), GsonPreset.CONFIG));
        mds.runOnce(ScanStage.DOWNLOAD, (path, iwModDescription) -> info.setState("Scanned " + path));
    }

    @Override
    public String getName() {
        return "Running mod discovery system";
    }
}
