package io.gitlab.jfronny.inceptum.launcher.system.importer;

import io.gitlab.jfronny.inceptum.common.GsonPreset;
import io.gitlab.jfronny.inceptum.common.Net;
import io.gitlab.jfronny.inceptum.launcher.model.modrinth.GC_ModrinthModpackManifest;
import io.gitlab.jfronny.inceptum.launcher.model.modrinth.ModrinthModpackManifest;
import io.gitlab.jfronny.inceptum.launcher.util.ProcessState;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;

public class ModrinthImporter extends Importer<ModrinthModpackManifest> {
    public ModrinthImporter() {
        super("Modrinth", "modrinth.index.json", path -> GC_ModrinthModpackManifest.deserialize(path, GsonPreset.API));
    }

    @Override
    protected IntermediaryManifest validateManifest(ModrinthModpackManifest manifest, Path sourceRoot) throws IOException {
        if (manifest.formatVersion() != 1) throw new IOException("Unsupported Modrinth modpack manifest version");
        if (!"minecraft".equals(manifest.game())) throw new IOException("Invalid input: not a minecraft modpack");
        if (manifest.dependencies() == null) throw new IOException("Modrinth manifest lacks dependency block");
        if (manifest.dependencies().minecraft() == null) throw new IOException("Could not find minecraft version");
        if (manifest.dependencies().fabricLoader() == null) {
            for (ModrinthModpackManifest.File file : manifest.files()) {
                if (file.path() == null) {
                    throw new IOException("File lacks path");
                }
                if (file.path().startsWith("mods")) {
                    throw new IOException("Found mod files but no fabric loader. Other loaders are unsupported!");
                }
            }
        }
        return new IntermediaryManifest(
                manifest.name(),
                "overrides",
                manifest.dependencies().minecraft(),
                manifest.dependencies().fabricLoader()
        );
    }

    @Override
    protected void downloadMods(ModrinthModpackManifest manifest, Path instanceDir, ProcessState state) throws IOException {
        if (manifest.files() != null) {
            filesLoop: for (ModrinthModpackManifest.File file : manifest.files()) {
                Path path = instanceDir.resolve(file.path()).toAbsolutePath().normalize();
                if (!path.startsWith(instanceDir)) throw new IOException("Pack attempted to write file outside instance which is dangerous and thus unsupported: " + path);
                String sha1 = file.hashes().sha1();
                IOException failedDownload = new IOException("Could not download file");
                for (String url : file.downloads()) {
                    try {
                        Net.downloadFile(url, sha1, path);
                        continue filesLoop;
                    } catch (IOException | URISyntaxException e) {
                        failedDownload.addSuppressed(e);
                    }
                }
                throw failedDownload;
            }
        }
    }
}
