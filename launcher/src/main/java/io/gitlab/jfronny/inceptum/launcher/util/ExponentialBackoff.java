package io.gitlab.jfronny.inceptum.launcher.util;

/**
 * A simple class to manage exponential backoff.
 * Used within the MDS to manage task retries.
 */
public class ExponentialBackoff {
    private int retries = 0;
    private long nextRetry = 0;

    public boolean shouldTry() {
        return retries == 0 || System.currentTimeMillis() > nextRetry;
    }

    public void sleep() {
        try {
            long time = nextRetry - System.currentTimeMillis();
            if (time > 0) Thread.sleep(time);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public void success() {
        retries = 0;
    }

    public void fail() {
        nextRetry = System.currentTimeMillis() + (1L << retries) * 1000;
        retries++;
    }
}
