package io.gitlab.jfronny.inceptum.launcher.model.fabric;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;

import java.util.List;

@GSerializable
public class FabricVersionLoaderInfo {
    public FabricLoaderVersion loader;
    public IntermediaryVersion intermediary;

    @GSerializable
    public static class WithMeta extends FabricVersionLoaderInfo {
        public LauncherMeta launcherMeta;

        @GSerializable
        public record LauncherMeta(int version, Libraries libraries, MainClass mainClass) {
            @GSerializable
            public record Libraries(List<Library> client, List<Library> common, List<Library> server) {
                @GSerializable
                public record Library(String name, String url) {
                }
            }

            @GSerializable
            public record MainClass(String client, String server) {
            }
        }
    }
}
