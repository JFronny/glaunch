package io.gitlab.jfronny.inceptum.launcher.system.launch;

import io.gitlab.jfronny.inceptum.launcher.api.FabricMetaApi;

public enum LaunchType {
    Server("server", FabricMetaApi.FabricVersionInfoType.Server), Client("client", FabricMetaApi.FabricVersionInfoType.Client);

    LaunchType(String name, FabricMetaApi.FabricVersionInfoType fabricMetaType) {
        this.name = name;
        this.fabricMetaType = fabricMetaType;
    }

    public final String name;
    public final FabricMetaApi.FabricVersionInfoType fabricMetaType;
}
