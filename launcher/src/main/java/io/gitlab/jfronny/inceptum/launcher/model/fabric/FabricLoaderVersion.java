package io.gitlab.jfronny.inceptum.launcher.model.fabric;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;

@GSerializable
public record FabricLoaderVersion(String separator, int build, String maven, String version, boolean stable) {
}
