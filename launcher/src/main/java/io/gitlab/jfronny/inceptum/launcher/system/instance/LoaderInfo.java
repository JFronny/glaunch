package io.gitlab.jfronny.inceptum.launcher.system.instance;

import io.gitlab.jfronny.inceptum.launcher.model.fabric.FabricLoaderVersion;

public record LoaderInfo(Type type, String version) {
    public static LoaderInfo NONE = new LoaderInfo(Type.None, "");

    public enum Type {
        None, Fabric
    }

    public LoaderInfo(FabricLoaderVersion fabricVersion) {
        this(LoaderInfo.Type.Fabric, fabricVersion.version());
    }
}
