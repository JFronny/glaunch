package io.gitlab.jfronny.inceptum.launcher.system.mds;

public enum ScanStage implements Comparable<ScanStage> {
    /**
     * The scanner has not yet scanned the mod(s) or it has been invalidated/deleted
     */
    NONE,
    /**
     * The mod(s) have been discovered and their metadata has been loaded
     */
    DISCOVER,
    /**
     * The mod(s) have been validated (and downloaded if necessary) and are ready to be used
     */
    DOWNLOAD,
    /**
     * The mod(s) have been cross-referenced with the mod sources
     */
    CROSSREFERENCE,
    /**
     * The mod(s) have been checked for updates
     */
    UPDATECHECK,
    /**
     * The mod(s) have been scanned in all stages
     */
    ALL;

    public boolean contains(ScanStage stage) {
        return ordinal() >= stage.ordinal();
    }

    public void require(ScanStage required) {
        if (ordinal() < required.ordinal()) {
            throw new NotYetScannedException(required, this);
        }
    }
}
