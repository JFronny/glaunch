package io.gitlab.jfronny.inceptum.launcher.model.fabric;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;

@GSerializable
public record FabricModJson(String id, String name, String description, String version) {
}
