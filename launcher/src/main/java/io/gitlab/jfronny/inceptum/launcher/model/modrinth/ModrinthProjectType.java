package io.gitlab.jfronny.inceptum.launcher.model.modrinth;

public enum ModrinthProjectType {
    mod, modpack
}
