package io.gitlab.jfronny.inceptum.launcher.util;

public class GameVersionParser {
    private static final String FABRIC_PREFIX = "fabric-loader-";

    public static boolean isFabric(String version) {
        return version.startsWith(FABRIC_PREFIX);
    }

    public static String getGameVersion(String version) {
        if (!isFabric(version)) return version;
        version = version.substring(FABRIC_PREFIX.length());
        return version.substring(version.indexOf('-') + 1);
    }

    public static String getLoaderVersion(String version) {
        if (!isFabric(version)) return null;
        version = version.substring(FABRIC_PREFIX.length());
        return version.substring(0, version.indexOf('-'));
    }

    public static String createVersionWithFabric(String gameVersion, String fabricVersion) {
        return FABRIC_PREFIX + fabricVersion + '-' + gameVersion;
    }
}
