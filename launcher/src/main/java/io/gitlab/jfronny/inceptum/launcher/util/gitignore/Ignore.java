package io.gitlab.jfronny.inceptum.launcher.util.gitignore;

import java.util.*;

public class Ignore {
    private final List<IgnoreRule> rules = new ArrayList<>();
    private final List<String> originalRules = new ArrayList<>();

    public Ignore add(String rule) {
        originalRules.add(rule);
        rules.add(new IgnoreRule(rule));
        return this;
    }

    public Ignore add(Collection<String> patterns) {
        originalRules.addAll(patterns);
        patterns.forEach(s -> rules.add(new IgnoreRule(s)));
        return this;
    }

    public Ignore add(String... patterns) {
        originalRules.addAll(Arrays.asList(patterns));
        for (String pattern : patterns) {
            rules.add(new IgnoreRule(pattern));
        }
        return this;
    }

    public boolean isIgnored(String path) {
        boolean ignore = false;
        for (IgnoreRule rule : rules) {
            if (rule.negate) {
                if (ignore && rule.isMatch(path)) {
                    ignore = false;
                }
            } else if (!ignore && rule.isMatch(path)) {
                ignore = true;
            }
        }
        return ignore;
    }
}
