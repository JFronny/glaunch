package io.gitlab.jfronny.inceptum.launcher.model.modrinth;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;

import java.util.Date;
import java.util.List;

@GSerializable
public record ModrinthProject(
        String id,
        String slug,
        ModrinthProjectType project_type,
        String team,
        String title,
        String description,
        String body,
        String body_url,
        Date publish,
        Date updated,
        String status,
        License license,
        ModrinthDependencyType client_side,
        ModrinthDependencyType server_side,
        long downloads,
        long follows,
        List<String> categories,
        List<String> versions,
        String icon_url,
        String issues_url,
        String source_url,
        String wiki_url,
        String discord_url,
        List<DonationUrl> donation_urls,
        List<GalleryItem> gallery
) {
    @GSerializable
    public record DonationUrl(String id, String platform, String url) {
    }

    @GSerializable
    public record License(String id, String name, String url) {
    }

    @GSerializable
    public record GalleryItem(String url, boolean featured, String title, String description, Date created) {
    }
}
