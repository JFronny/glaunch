package io.gitlab.jfronny.inceptum.launcher.model.curseforge.response;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;
import io.gitlab.jfronny.inceptum.launcher.model.curseforge.CurseforgeMod;

import java.util.List;

@GSerializable
public record SearchResponse(List<CurseforgeMod> data, Pagination pagination) {
    @GSerializable
    public record Pagination(int index, int pageSite, int resultCount, int totalCount) {
    }
}
