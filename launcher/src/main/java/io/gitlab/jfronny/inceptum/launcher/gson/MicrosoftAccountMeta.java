package io.gitlab.jfronny.inceptum.launcher.gson;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;
import io.gitlab.jfronny.inceptum.launcher.model.microsoft.response.OauthTokenResponse;
import io.gitlab.jfronny.inceptum.launcher.model.microsoft.response.XboxLiveAuthResponse;

import java.util.Date;

@GSerializable
public record MicrosoftAccountMeta(String accountId, String minecraftUsername, String uuid, String accessToken, OauthTokenResponse oauthToken, XboxLiveAuthResponse xstsAuth, Date accessTokenExpiresAt, boolean mustLogin) {
}
