package io.gitlab.jfronny.inceptum.launcher.model.curseforge.response;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;

@GSerializable
public record GetModDescriptionResponse(String data) {
}
