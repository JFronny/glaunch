package io.gitlab.jfronny.inceptum.launcher.system.source;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;
import io.gitlab.jfronny.inceptum.common.MetaHolder;
import io.gitlab.jfronny.inceptum.launcher.gson.ModSourceAdapter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Optional;
import java.util.Set;

@GSerializable(with = ModSourceAdapter.class)
public interface ModSource {
    ModDownload download() throws IOException;

    Set<ModSource> getDependencies(String gameVersion) throws IOException;

    Optional<ModSource> getUpdate(String gameVersion) throws IOException;

    String getVersion();

    String getName();

    String getShortName();

    String getFileName();

    String getDescription();

    String getSummary();

    boolean equals(ModSource other);

    boolean projectMatches(ModSource other);

    default Path getJarPath() {
        return MetaHolder.LIBRARIES_DIR.resolve("com").resolve(getName()).resolve(getFileName());
    }
}
