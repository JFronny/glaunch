package io.gitlab.jfronny.inceptum.launcher.model.inceptum;

import io.gitlab.jfronny.commons.data.MutCollection;
import io.gitlab.jfronny.commons.data.delegate.DelegateMap;
import io.gitlab.jfronny.commons.io.HashUtils;
import io.gitlab.jfronny.commons.serialize.generator.annotations.GPrefer;
import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.Utils;
import io.gitlab.jfronny.inceptum.launcher.api.CurseforgeApi;
import io.gitlab.jfronny.inceptum.launcher.api.ModrinthApi;
import io.gitlab.jfronny.inceptum.launcher.gson.ModMetaSourcesAdapter;
import io.gitlab.jfronny.inceptum.launcher.model.curseforge.response.FingerprintMatchesResponse;
import io.gitlab.jfronny.inceptum.launcher.system.source.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

@GSerializable
public record ModMeta(
        Sources sources, //key: source, value: update
        String sha1,
        Long murmur2,
        List<String> dependents, // by file name
        List<String> dependencies, // by file name
        boolean explicit
) {
    @GSerializable(with = ModMetaSourcesAdapter.class)
    public static class Sources extends DelegateMap<ModSource, Optional<ModSource>> {
        public Sources() {
            super(MutCollection.mapOf());
        }

        private Optional<ModSource> getPreferredMetadataSource() {
            return keySet().stream().max((left, right) -> {
                if (left.equals(right)) return 0;
                if (left instanceof ModrinthModSource) return 1;
                if (right instanceof ModrinthModSource) return -1;
                if (left instanceof CurseforgeModSource) return 1;
                if (right instanceof CurseforgeModSource) return -1;
                return 1;
            });
        }

        public @NotNull String getBestSummary() {
            return getPreferredMetadataSource()
                    .map(ModSource::getSummary)
                    .orElse("Local Mod");
        }

        public @Nullable String getBestDescription() {
            return getPreferredMetadataSource()
                    .map(ModSource::getDescription)
                    .orElse(null);
        }
    }

    @GPrefer
    public ModMeta {}

    public static ModMeta fromJar(Path mod) {
        String sha1 = null;
        Long murmur2 = null;
        if (!Files.isDirectory(mod)) {
            try {
                byte[] data = Files.readAllBytes(mod);
                sha1 = HashUtils.sha1(data);
                murmur2 = HashUtils.murmur2(data);
            } catch (IOException e) {
                Utils.LOGGER.error("Could not read file hash", e);
            }
        }
        return new ModMeta(
                new Sources(),
                sha1,
                murmur2,
                new ArrayList<>(),
                new ArrayList<>(),
                true
        );
    }

    public static ModMeta fromDownload(ModDownload download, @Nullable ModSource knownSource, String gameVersion) {
        ModMeta res = new ModMeta(
                new Sources(),
                download.sha1(),
                download.murmur2(),
                new ArrayList<>(),
                new ArrayList<>(),
                true
        );
        if (knownSource != null) res.checkAndAddSource(knownSource, gameVersion);
        res.updateCheck(gameVersion);
        return res;
    }

    public boolean crossReference() {
        boolean modrinth = false;
        boolean curseforge = false;
        for (ModSource source : sources.keySet().toArray(ModSource[]::new)) {
            if (source instanceof ModrinthModSource) modrinth = true;
            if (source instanceof CurseforgeModSource) curseforge = true;
        }
        boolean changed = false;
        if (!modrinth) {
            try {
                sources.put(new ModrinthModSource(ModrinthApi.getVersionByHash(sha1).id()), Optional.empty());
                changed = true;
            } catch (IOException e) {
                // not found
            }
        }
        if (!curseforge) {
            try {
                FingerprintMatchesResponse.Result cf = CurseforgeApi.checkFingerprint(murmur2);
                if (!cf.exactMatches().isEmpty()) {
                    FingerprintMatchesResponse.Result.Match f = cf.exactMatches().getFirst();
                    sources.put(new CurseforgeModSource(f.id(), f.file().id()), Optional.empty());
                    changed = true;
                }
            } catch (IOException | URISyntaxException e) {
                // not found
            }
        }
        return changed;
    }

    public void updateCheck(String gameVersion) {
        for (ModSource source : sources.keySet().toArray(ModSource[]::new)) {
            checkAndAddSource(source, gameVersion);
        }
    }

    private void checkAndAddSource(ModSource source, String gameVersion) {
        try {
            sources.put(source, source.getUpdate(gameVersion));
        } catch (IOException e) {
            Utils.LOGGER.error("Could not check " + source.getName() + " for updates", e);
        }
    }
}
