package io.gitlab.jfronny.inceptum.launcher.model.curseforge.response;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;
import io.gitlab.jfronny.inceptum.launcher.model.curseforge.CurseforgeFile;

@GSerializable
public record GetModFileResponse(CurseforgeFile data) {
}
