package io.gitlab.jfronny.inceptum.launcher.model.curseforge.response;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;
import io.gitlab.jfronny.inceptum.launcher.model.curseforge.CurseforgeFile;

import java.util.List;

@GSerializable
public record FingerprintMatchesResponse(Result data) {
    @GSerializable
    public record Result(boolean isCacheBuilt,
                         List<Match> exactMatches,
                         List<Integer> exactFingerprints,
                         List<Match> partialMatches,
                         List<Integer> installedFingerprints,
                         List<Integer> unmatchedFingerprints) {
        @GSerializable
        public record Match(int id, CurseforgeFile file, List<CurseforgeFile> latestFiles) {
        }
    }
}
