package io.gitlab.jfronny.inceptum.launcher.model.mojang;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;
import io.gitlab.jfronny.inceptum.launcher.gson.RulesAdapter;

@GSerializable(with = RulesAdapter.class)
public record Rules(boolean allow) implements Cloneable {
    @Override
    protected Rules clone() {
        return new Rules(allow);
    }
}
