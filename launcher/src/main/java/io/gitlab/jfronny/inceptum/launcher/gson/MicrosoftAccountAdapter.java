package io.gitlab.jfronny.inceptum.launcher.gson;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.inceptum.launcher.api.account.MicrosoftAccount;

public class MicrosoftAccountAdapter {
    public static <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(MicrosoftAccount value, Writer writer) throws TEx, MalformedDataException {
        GC_MicrosoftAccountMeta.serialize(value == null ? null : value.toMeta(), writer);
    }

    public static <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> MicrosoftAccount deserialize(Reader reader) throws TEx, MalformedDataException {
        MicrosoftAccountMeta meta = GC_MicrosoftAccountMeta.deserialize(reader);
        return meta == null ? null : new MicrosoftAccount(meta);
    }
}
