package io.gitlab.jfronny.inceptum.launcher.system.importer;

import io.gitlab.jfronny.inceptum.common.GsonPreset;
import io.gitlab.jfronny.inceptum.launcher.model.multimc.GC_MMCPackMeta;
import io.gitlab.jfronny.inceptum.launcher.model.multimc.MMCPackMeta;
import io.gitlab.jfronny.inceptum.launcher.util.ProcessState;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

public class MultiMCImporter extends Importer<MMCPackMeta> {
    public MultiMCImporter() {
        super("MultiMC", "mmc-pack.json", path -> GC_MMCPackMeta.deserialize(path, GsonPreset.API));
    }

    @Override
    protected IntermediaryManifest validateManifest(MMCPackMeta manifest, Path sourceRoot) throws IOException {
        if (manifest.formatVersion() != 1) throw new IOException("Unsupported MultiMC format version");
        if (manifest.components() == null) throw new IOException("MultiMC pack missing components");
        String gameVersion = null;
        String fabricVersion = null;
        String name = null;
        Path instanceCfg = sourceRoot.resolve("instance.cfg");
        if (Files.exists(instanceCfg)) {
            try (Stream<String> lines = Files.lines(instanceCfg)) {
                final String namePrefix = "name=";
                name = lines.filter(l -> l.startsWith(namePrefix))
                        .map(l -> l.substring(namePrefix.length()))
                        .findFirst()
                        .orElse(null);
            }
        }
        for (MMCPackMeta.Component component : manifest.components()) {
            if ("net.minecraft".equals(component.uid())) gameVersion = component.version();
            if ("net.fabricmc.fabric-loader".equals(component.uid())) fabricVersion = component.version();
        }
        if (gameVersion == null) throw new IOException("Pack lacks minecraft component");
        return new IntermediaryManifest(name, ".minecraft", gameVersion, fabricVersion);
    }

    @Override
    protected void downloadMods(MMCPackMeta manifest, Path instanceDir, ProcessState state) throws IOException {
        // All mods are overrides, so do nothing
    }
}
