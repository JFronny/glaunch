package io.gitlab.jfronny.inceptum.launcher.gson;

import io.gitlab.jfronny.commons.OSUtils;
import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.inceptum.launcher.model.mojang.Rules;

public class RulesAdapter {
    public static <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(Rules rules, Writer writer) throws TEx {
        throw new UnsupportedOperationException();
    }

    public static <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> Rules deserialize(Reader reader) throws TEx, MalformedDataException {
        boolean valid = true;
        reader.beginArray();
        while (reader.hasNext()) {
            if (!valid) {
                reader.skipValue();
                continue;
            }
            reader.beginObject();
            String actionType = null;
            boolean hasFeatures = false;
            String osName = null;
            String osVersion = null;
            while (reader.hasNext()) {
                switch (reader.nextName()) {
                    case "action" -> actionType = reader.nextString();
                    case "features" -> {
                        reader.skipValue();
                        hasFeatures = true;
                    }
                    case "os" -> {
                        reader.beginObject();
                        while (reader.hasNext()) {
                            switch (reader.nextName()) {
                                case "name" -> osName = reader.nextString();
                                case "version" -> osVersion = reader.nextString();
                                default -> reader.skipValue();
                            }
                        }
                        reader.endObject();
                    }
                }
            }
            reader.endObject();
            if (actionType == null || (!actionType.equals("allow") && !actionType.equals("disallow"))) {
                throw new MalformedDataException("Unexpected action in argument: " + actionType);
            }
            if (hasFeatures) valid = false;
            if (osName != null && !OSUtils.TYPE.mojName.equals(osName)) valid = false;
            if (osVersion != null && !System.getProperty("os.version").matches(osVersion)) valid = false;
            if (actionType.equals("disallow")) valid = !valid;
        }
        reader.endArray();
        return new Rules(valid);
    }
}
