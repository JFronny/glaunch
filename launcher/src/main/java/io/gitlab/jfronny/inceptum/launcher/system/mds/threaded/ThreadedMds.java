package io.gitlab.jfronny.inceptum.launcher.system.mds.threaded;

import io.gitlab.jfronny.commons.io.JFiles;
import io.gitlab.jfronny.commons.ref.R;
import io.gitlab.jfronny.commons.throwable.ThrowingBiConsumer;
import io.gitlab.jfronny.inceptum.common.Utils;
import io.gitlab.jfronny.inceptum.launcher.model.inceptum.InstanceMeta;
import io.gitlab.jfronny.inceptum.launcher.system.mds.*;
import io.gitlab.jfronny.inceptum.launcher.system.instance.ModPath;
import io.gitlab.jfronny.inceptum.launcher.system.mds.noop.NoopMod;
import io.gitlab.jfronny.inceptum.launcher.util.GameVersionParser;

import java.io.Closeable;
import java.io.IOException;
import java.nio.file.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.BiConsumer;

import static java.nio.file.StandardWatchEventKinds.*;

public class ThreadedMds implements ModsDirScanner {
    private static final Map<Path, ThreadedMds> SCANNERS = new HashMap<>();
    private static final ExecutorService POOL = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors(), new NamedThreadFactory("mds"));
    private final Map<Path, Mod> descriptions = new HashMap<>();
    private final Set<Path> scannedPaths = new HashSet<>();
    private final Thread th;
    private final ProtoInstance instance;
    private final WatchService service;
    private boolean disposed = false;

    private ThreadedMds(Path modsDir, InstanceMeta instance) throws IOException {
        this.instance = new ProtoInstance(modsDir, this, instance);
        this.th = new Thread(this::scanTaskInternal, "mds-" + modsDir.getParent().getFileName());
        this.service = FileSystems.getDefault().newWatchService();
        modsDir.register(service, ENTRY_MODIFY, ENTRY_CREATE, ENTRY_DELETE);
    }

    public static ThreadedMds get(Path modsDir, InstanceMeta instance) throws IOException {
        if (SCANNERS.containsKey(modsDir)) {
            ThreadedMds mds = SCANNERS.get(modsDir);
            if (mds.instance.meta().equals(instance)) return mds;
            mds.close();
        }
        ThreadedMds mds = new ThreadedMds(modsDir, instance);
        SCANNERS.put(modsDir, mds);
        return mds;
    }

    @Override
    public boolean isComplete(ScanStage stage) {
        if (!Files.isDirectory(instance.modsDir())) return true;
        try {
            for (Path path : JFiles.list(instance.modsDir())) {
                if (!descriptions.containsKey(path)) return false;
            }
        } catch (IOException e) {
            Utils.LOGGER.error("Could not list files in mods dir", e);
        }
        return true;
    }

    @Override
    public void start() {
        if (!th.isAlive()) th.start();
    }

    @Override
    public Closeable focus() {
        return R::nop;
    }

    @Override
    public String getGameVersion() {
        return GameVersionParser.getGameVersion(instance.meta().gameVersion);
    }

    @Override
    public Set<Mod> getMods() throws IOException {
        Set<Mod> mods = new TreeSet<>();
        if (Files.isDirectory(instance.modsDir())) {
            for (Path path : JFiles.list(instance.modsDir())) {
                if (ModPath.isImod(path) && Files.exists(ModPath.trimImod(path)))
                    continue;
                mods.add(get(path));
            }
        }
        return mods;
    }

    @Override
    public Mod get(Path path) {
        if (!Files.isRegularFile(path)) return null;
        if (!descriptions.containsKey(path)) return new NoopMod(path); // not yet scanned
        return descriptions.get(path);
    }

    @Override
    public void invalidate(Path path) {
        descriptions.remove(path);
        scannedPaths.remove(path);
    }

    public boolean hasScanned(Path path) {
        return scannedPaths.contains(path)
                || scannedPaths.contains(path.getParent().resolve(path.getFileName().toString() + ".imod"));
    }

    private void scanTaskInternal() {
        while (!disposed) {
            runOnce(ScanStage.UPDATECHECK, R::nop);
        }
    }

    @Override
    public void runOnce(ScanStage targetStage, ThrowingBiConsumer<Path, Mod, IOException> discovered) {
        try {
            if (!Files.isDirectory(instance.modsDir())) {
                return;
            }
            Set<Future<?>> tasks = new HashSet<>();
            discovered = discovered.andThen((path, iwModDescription) -> {
                scannedPaths.add(path);
                descriptions.put(path, iwModDescription);
            });
            Set<Path> toScan;
            if (descriptions.isEmpty()) {
                toScan = Set.copyOf(JFiles.list(instance.modsDir()));
            } else {
                toScan = new HashSet<>();
                WatchKey key = service.poll();
                if (key != null) {
                    for (WatchEvent<?> event : key.pollEvents()) {
                        if (event.context() instanceof Path p) {
                            toScan.add(instance.modsDir().resolve(p));
                        }
                    }
                    if (!key.reset()) Utils.LOGGER.warn("Could not reset config watch key");
                }
                JFiles.listTo(instance.modsDir(), path -> {
                    if (!descriptions.containsKey(path)) toScan.add(path);
                });
            }
            for (Path p : toScan) {
                tasks.add(POOL.submit(new FileScanTask(instance, p, discovered, getGameVersion())));
            }
            for (Future<?> task : tasks) {
                try {
                    task.get();
                } catch (ExecutionException e) {
                    Utils.LOGGER.error("Failed to execute ScanTask", e);
                }
            }
        } catch (IOException | InterruptedException e) {
            Utils.LOGGER.error("Could not list mods", e);
        }
    }

    public static void closeAll() {
        for (ThreadedMds value : SCANNERS.values().toArray(ThreadedMds[]::new)) {
            try {
                value.close();
            } catch (IOException e) {
                Utils.LOGGER.error("Could not close MDS", e);
            }
        }
        POOL.shutdown();
    }

    @Override
    public void close() throws IOException {
        disposed = true;
        service.close();
        SCANNERS.remove(instance.modsDir());
    }
}
