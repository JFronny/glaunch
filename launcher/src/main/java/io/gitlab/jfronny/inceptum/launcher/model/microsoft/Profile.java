package io.gitlab.jfronny.inceptum.launcher.model.microsoft;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;

import java.util.List;

@GSerializable
public record Profile(String id, String name, List<Skin> skins, List<Cape> capes) {
    @GSerializable
    public record Skin(String id, String state, String url, String variant, String alias) {
    }

    @GSerializable
    public record Cape(String id) {
    }
}
