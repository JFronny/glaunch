package io.gitlab.jfronny.inceptum.launcher.model.modrinth;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;

@GSerializable
public record ModrinthHashes(String sha1, String sha512) {
}
