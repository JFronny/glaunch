package io.gitlab.jfronny.inceptum.launcher.model.modrinth;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;

import java.util.Date;
import java.util.List;

@GSerializable
public record ModrinthVersion(
        String id,
        String project_id,
        String author_id,
        boolean featured,
        String name,
        String version_number,
        String changelog,
        String changelog_url,
        Date date_published,
        long downloads,
        VersionType version_type,
        List<File> files,
        List<Dependency> dependencies,
        List<String> game_versions,
        List<String> loaders
) {
    public enum VersionType {
        alpha, beta, release
    }

    @GSerializable
    public record File(ModrinthHashes hashes, String url, String filename, boolean primary) {
    }

    @GSerializable
    public record Dependency(String version_id, String project_id, DependencyType dependency_type) {
        public enum DependencyType {
            required,
            optional,
            incompatible
        }
    }
}
