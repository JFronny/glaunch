package io.gitlab.jfronny.inceptum.launcher.system.mds.flow;

import io.gitlab.jfronny.commons.http.client.HttpClient;
import io.gitlab.jfronny.commons.throwable.ThrowingConsumer;
import io.gitlab.jfronny.inceptum.launcher.system.mds.*;

import java.io.IOException;

public record MdsUpdateTask(ProtoInstance instance, String gameVersion) implements ThrowingConsumer<MdsMod, IOException> {
    @Override
    public void accept(MdsMod mod) throws IOException {
        if (mod.getScanStage().contains(ScanStage.UPDATECHECK)) return;
        if (HttpClient.wasOnline()) {
            mod.getMetadata().updateCheck(gameVersion);
            mod.markUpdateChecked();
        }
    }
}
