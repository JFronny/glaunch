package io.gitlab.jfronny.inceptum.launcher.system.setup;

import io.gitlab.jfronny.commons.io.JFiles;
import io.gitlab.jfronny.inceptum.common.MetaHolder;
import io.gitlab.jfronny.inceptum.common.Utils;
import io.gitlab.jfronny.inceptum.launcher.model.mojang.VersionInfo;
import io.gitlab.jfronny.inceptum.launcher.system.instance.Instance;
import io.gitlab.jfronny.inceptum.launcher.system.instance.LoaderInfo;
import io.gitlab.jfronny.inceptum.launcher.util.ProcessState;

import java.io.IOException;
import java.nio.file.Files;

public record SetupStepInfo(VersionInfo version,
                            LoaderInfo loader,
                            String name,
                            ProcessState currentState) {
    public void setState(String state) {
        currentState.updateStep(state);
    }

    public boolean isCancelled() {
        return currentState.isCancelled();
    }

    public void tryRemoveInstance() {
        try {
            removeInstance();
        } catch (IOException e) {
            Utils.LOGGER.error("Could not delete instance dir", e);
        }
    }

    public void removeInstance() throws IOException {
        JFiles.deleteRecursive(MetaHolder.INSTANCE_DIR.resolve(name));
    }

    public void clearSetupLock() {
        try {
            Files.deleteIfExists(MetaHolder.INSTANCE_DIR.resolve(name).resolve(Instance.SETUP_LOCK_NAME));
        } catch (IOException ignored) {
        }
    }
}
