package io.gitlab.jfronny.inceptum.launcher.system.mds.flow;

import io.gitlab.jfronny.commons.io.JFiles;
import io.gitlab.jfronny.commons.ref.R;
import io.gitlab.jfronny.commons.throwable.ThrowingBiConsumer;
import io.gitlab.jfronny.commons.tuple.Tuple;
import io.gitlab.jfronny.inceptum.common.Utils;
import io.gitlab.jfronny.inceptum.launcher.model.inceptum.InstanceMeta;
import io.gitlab.jfronny.inceptum.launcher.system.instance.ModPath;
import io.gitlab.jfronny.inceptum.launcher.system.mds.*;
import io.gitlab.jfronny.inceptum.launcher.system.mds.noop.NoopMod;
import io.gitlab.jfronny.inceptum.launcher.util.*;

import java.io.IOException;
import java.nio.file.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.nio.file.StandardWatchEventKinds.*;

public class FlowMds implements ModsDirScanner {
    private static final Map<Path, FlowMds> SCANNERS = new HashMap<>();
    private boolean disposed = false;

    private final MdsThreadFactory factory = new MdsThreadFactory("mds");

    private final ProtoInstance instance;
    private final WatchService service;
    private final Thread th;
    private final Map<Path, Mod> descriptions = new HashMap<>();
    private final Set<Path> scannedPaths = new HashSet<>();

    private FlowMds(Path modsDir, InstanceMeta instance) throws IOException {
        this.instance = new ProtoInstance(modsDir, this, instance);
        this.th = factory.newThread(this::scanTaskInternal, modsDir.getParent().getFileName().toString());
        this.service = FileSystems.getDefault().newWatchService();
        modsDir.register(service, ENTRY_MODIFY, ENTRY_CREATE, ENTRY_DELETE);
    }

    public static FlowMds get(Path modsDir, InstanceMeta instance) throws IOException {
        if (SCANNERS.containsKey(modsDir)) {
            FlowMds mds = SCANNERS.get(modsDir);
            if (mds.instance.meta().equals(instance)) return mds;
            mds.close();
        }
        FlowMds mds = new FlowMds(modsDir, instance);
        SCANNERS.put(modsDir, mds);
        return mds;
    }

    @Override
    public boolean isComplete(ScanStage stage) {
        if (!Files.isDirectory(instance.modsDir())) return true;
        try {
            for (Path path : JFiles.list(instance.modsDir())) {
                Mod mod = descriptions.get(path);
                if (mod == null || mod.getScanStage().ordinal() < stage.ordinal()) return false;
            }
        } catch (IOException e) {
            Utils.LOGGER.error("Could not list files in mods dir", e);
        }
        return true;
    }

    @Override
    public void start() {
        if (!th.isAlive()) th.start();
    }

    @Override
    public VoidClaimPool.Claim focus() {
        return factory.focusClaim.claim();
    }

    @Override
    public String getGameVersion() {
        return GameVersionParser.getGameVersion(instance.meta().gameVersion);
    }

    @Override
    public Set<Mod> getMods() throws IOException {
        Set<Mod> mods = new TreeSet<>();
        if (Files.isDirectory(instance.modsDir())) {
            for (Path path : JFiles.list(instance.modsDir())) {
                if (ModPath.isImod(path) && Files.exists(ModPath.trimImod(path)))
                    continue;
                mods.add(get(path));
            }
        }
        return mods;
    }

    @Override
    public Mod get(Path path) {
        if (!Files.isRegularFile(path)) return null;
        if (!descriptions.containsKey(path)) return new NoopMod(path); // not yet scanned
        return descriptions.get(path);
    }

    @Override
    public void invalidate(Path path) {
        descriptions.remove(path);
        scannedPaths.remove(path);
    }

    @Override
    public boolean hasScanned(Path path) {
        return scannedPaths.contains(path) || scannedPaths.contains(ModPath.appendImod(path));
    }

    private final Set<SavedTask> savedTasks = new HashSet<>();
    record SavedTask(ScanStage at, ThrowingBiConsumer<Path, Mod, IOException> discovered, CompletableFuture<Void> future) implements ThrowingBiConsumer<Path, Mod, IOException> {
        @Override
        public void accept(Path var1, Mod var2) throws IOException {
            try {
                discovered.accept(var1, var2);
                future.complete(null);
            } catch (Throwable e) {
                future.completeExceptionally(e);
            }
        }
    }

    private void waitUntilAfterScan(ScanStage at, ThrowingBiConsumer<Path, Mod, IOException> discovered) {
        CompletableFuture<Void> future = new CompletableFuture<>();
        synchronized (savedTasks) {
            savedTasks.add(new SavedTask(at, discovered, future));
        }
        try {
            future.get();
        } catch (InterruptedException | ExecutionException e) {
            Utils.LOGGER.error("Could not wait for scan to finish", e);
        }
    }

    private void scanTaskInternal() {
        while (!disposed) {
            Map<ScanStage, SavedTask> tasks;
            synchronized (savedTasks) {
                tasks = savedTasks.stream().collect(Collectors.toMap(
                        SavedTask::at,
                        Function.identity()
                ));
                savedTasks.clear();
            }
            try {
                performScanTask(Map.of(ScanStage.ALL, R::nop));
                Thread.sleep(1000);
            } catch (IOException e) {
                for (SavedTask task : tasks.values()) {
                    task.future.completeExceptionally(e);
                }
            } catch (InterruptedException e) {
                for (SavedTask task : tasks.values()) {
                    task.future.completeExceptionally(e);
                }
                throw new RuntimeException(e);
            }
        }
    }

    private final HashMap<String, ExponentialBackoff> backoffs = new HashMap<>();
    private void performScanTask(Map<ScanStage, ThrowingBiConsumer<Path, Mod, IOException>> discovered) throws IOException {
        if (!Files.isDirectory(instance.modsDir())) {
            return;
        }
        MdsPipeline pipeline = new MdsPipeline();
        pipeline.addTask(ScanStage.NONE, (path, mod) -> {
            descriptions.put(path, mod);
            scannedPaths.add(path);
        });
        ScanStage targetStage = discovered.keySet().stream()
                .max(Comparator.naturalOrder())
                .orElse(ScanStage.DISCOVER);
        pipeline.addFeedbackTask(discovered.get(ScanStage.NONE));
        pipeline.addFeedbackTask(discovered.get(ScanStage.DISCOVER));
        if (targetStage.contains(ScanStage.DOWNLOAD)) {
            pipeline.addTask(ScanStage.DOWNLOAD, new MdsTaskRetryWrapper(backoffs, new MdsDownloadTask(instance)));
            pipeline.addFeedbackTask(discovered.get(ScanStage.DOWNLOAD));
        }
        if (targetStage.contains(ScanStage.CROSSREFERENCE)) {
            pipeline.addTask(ScanStage.CROSSREFERENCE, new MdsTaskRetryWrapper(backoffs, new MdsCrossReferenceTask(instance)));
            pipeline.addFeedbackTask(discovered.get(ScanStage.CROSSREFERENCE));
        }
        if (targetStage.contains(ScanStage.UPDATECHECK)) {
            pipeline.addTask(ScanStage.UPDATECHECK, new MdsTaskRetryWrapper(backoffs, new MdsUpdateTask(instance, getGameVersion())));
            pipeline.addFeedbackTask(discovered.get(ScanStage.UPDATECHECK));
        }
        pipeline.addFeedbackTask(discovered.get(ScanStage.ALL));
        var futures1 = pipeline.run(factory, getToScan(), new MdsDiscoverTask(backoffs, instance, getGameVersion()));
        var futures2 = pipeline.run(factory, descriptions.entrySet().stream().map(Tuple::from).collect(Collectors.toSet()));
        for (CompletableFuture<Void> future : Stream.concat(futures1.stream(), futures2.stream()).toList()) {
            try {
                future.get();
            } catch (InterruptedException | ExecutionException e) {
                Utils.LOGGER.error("Could not scan file for mod info", e);
            }
        }
    }

    private Set<Path> getToScan() throws IOException {
        if (descriptions.isEmpty()) return Set.copyOf(JFiles.list(instance.modsDir()));
        Set<Path> toScan = new HashSet<>();
        WatchKey key = service.poll();
        if (key != null) {
            for (WatchEvent<?> event : key.pollEvents()) {
                if (event.context() instanceof Path p) {
                    toScan.add(instance.modsDir().resolve(p));
                }
            }
            if (!key.reset()) Utils.LOGGER.error("Could not reset watch key");
        }
        JFiles.listTo(instance.modsDir(), path -> {
            if (!descriptions.containsKey(path)) toScan.add(path);
        });
        return toScan;
    }

    @Override
    public void runOnce(ScanStage targetStage, ThrowingBiConsumer<Path, Mod, IOException> discovered) {
        try {
            if (!th.isAlive()) {
                performScanTask(Map.of(targetStage, discovered));
            } else {
                waitUntilAfterScan(targetStage, discovered);
            }
        } catch (IOException e) {
            Utils.LOGGER.error("Could not scan file for mod info", e);
        }
    }

    public static void closeAll() {
        for (FlowMds value : SCANNERS.values().toArray(FlowMds[]::new)) {
            try {
                value.close();
            } catch (IOException e) {
                Utils.LOGGER.error("Could not close MDS", e);
            }
        }
        MdsThreadFactory.scheduler.shutdown();
    }

    @Override
    public void close() throws IOException {
        disposed = true;
        service.close();
        SCANNERS.remove(instance.modsDir());
    }
}
