package io.gitlab.jfronny.inceptum.launcher.system.setup.steps;

import io.gitlab.jfronny.commons.ComparableVersion;
import io.gitlab.jfronny.inceptum.common.*;
import io.gitlab.jfronny.inceptum.launcher.model.mojang.MojangFileDownload;
import io.gitlab.jfronny.inceptum.launcher.system.setup.SetupStepInfo;
import io.gitlab.jfronny.inceptum.launcher.system.setup.Step;
import io.gitlab.jfronny.inceptum.launcher.util.GameVersionParser;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.*;

public class DownloadClientStep implements Step {
    @Override
    public void execute(SetupStepInfo info) throws IOException {
        Path clientPath = MetaHolder.LIBRARIES_DIR.resolve("net/minecraft/client");
        Path serverPath = MetaHolder.LIBRARIES_DIR.resolve("net/minecraft/server");
        if (!Files.exists(clientPath)) Files.createDirectories(clientPath);
        if (!Files.exists(serverPath)) Files.createDirectories(serverPath);
        String minecraftVersion = GameVersionParser.getGameVersion(info.version().id);
        clientPath = clientPath.resolve(minecraftVersion + ".jar");
        serverPath = serverPath.resolve(minecraftVersion + ".jar");
        try {
            if (!Files.exists(clientPath)) {
                MojangFileDownload client = info.version().downloads.client();
                info.setState("Downloading Client");
                Net.downloadFile(client.url, client.sha1, clientPath);
            }
            Utils.LOGGER.info(serverPath.toString());
            if (!Files.exists(serverPath)) {
                MojangFileDownload server = info.version().downloads.server();
                if (new ComparableVersion(minecraftVersion).compareTo("1.18") >= 0) {
                    info.setState("Downloading Bundler");
                    Path p = Files.createTempFile("bundler", ".jar");
                    Net.downloadFile(server.url, server.sha1, p);
                    try (FileSystem fs = Utils.openZipFile(p, false)) {
                        Files.copy(fs.getPath("META-INF", "versions", minecraftVersion, "server-" + minecraftVersion + ".jar"),
                                serverPath);
                    }
                    Files.delete(p);
                } else {
                    info.setState("Downloading Server");
                    Net.downloadFile(server.url, server.sha1, serverPath);
                }
            }
        } catch (URISyntaxException e) {
            throw new IOException("Could not download client", e);
        }
    }

    @Override
    public String getName() {
        return "Downloading Game";
    }
}
