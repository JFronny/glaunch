package io.gitlab.jfronny.inceptum.launcher;

import io.gitlab.jfronny.commons.io.JFiles;
import io.gitlab.jfronny.commons.ref.R;
import io.gitlab.jfronny.inceptum.common.*;
import io.gitlab.jfronny.inceptum.launcher.api.McApi;
import io.gitlab.jfronny.inceptum.launcher.api.account.MicrosoftAccount;
import io.gitlab.jfronny.inceptum.launcher.system.mds.ModsDirScanner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Objects;
import java.util.function.Consumer;

public class LauncherEnv {
    private static EnvBackend backend;

    public static void initialize(EnvBackend backend) throws IOException {
        if (MetaHolder.class.getClassLoader() != LauncherEnv.class.getClassLoader()) {
            throw new IllegalStateException("Mismatching classloader between two classes. Something is wrong here");
        }

        updateBackend(backend);

        InceptumEnvironmentInitializer.initialize();

        if (!Files.exists(MetaHolder.CACHE_DIR)) Files.createDirectories(MetaHolder.CACHE_DIR);
        try {
            McApi.getVersions();
            JFiles.clearDirectory(MetaHolder.CACHE_DIR);
            McApi.getVersions(); // needed in UI, better to initialize it here
        } catch (IOException e) {
            Utils.LOGGER.error("Could not connect to the internet");
        }
        if (!Files.exists(MetaHolder.INSTANCE_DIR)) Files.createDirectories(MetaHolder.INSTANCE_DIR);
        if (!Files.exists(MetaHolder.ASSETS_DIR)) Files.createDirectories(MetaHolder.ASSETS_DIR);
        if (!Files.exists(MetaHolder.LIBRARIES_DIR)) Files.createDirectories(MetaHolder.LIBRARIES_DIR);
        if (Files.exists(MetaHolder.FORCE_LOAD_PATH)) {
            Utils.LOGGER.info("Force-Loading libraries:");
            System.setProperty("java.library.path", System.getProperty("java.library.path", "") + File.pathSeparator + MetaHolder.FORCE_LOAD_PATH);
            JFiles.listTo(MetaHolder.FORCE_LOAD_PATH, path -> {
                Utils.LOGGER.info("Loading " + path);
                try {
                    System.load(path.toAbsolutePath().toString());
                } catch (UnsatisfiedLinkError le) {
                    Utils.LOGGER.error("Could not load library", le);
                }
            });
        }
    }

    public static void updateBackend(EnvBackend backend) {
        LauncherEnv.backend = Objects.requireNonNull(backend);
    }

    public static void terminate() {
        ModsDirScanner.closeAll();
    }

    public static void showError(String message, String title) {
        backend.showError(message, title);
    }

    public static void showError(String message, Throwable t) {
        backend.showError(message, t);
    }

    public static void showInfo(String message, String title) {
        backend.showInfo(message, title);
    }

    public static void showOkCancel(String message, String title, Runnable ok) {
        showOkCancel(message, title, ok, R::nop);
    }

    public static void showOkCancel(String message, String title, Runnable ok, Runnable cancel) {
        backend.showOkCancel(message, title, ok, cancel);
    }

    public static void showOkCancel(String message, String title, Runnable ok, Runnable cancel, boolean defaultCancel) {
        backend.showOkCancel(message, title, ok, cancel, defaultCancel);
    }

    public static void getInput(String prompt, String details, String defaultValue, Consumer<String> ok, Runnable cancel) throws IOException {
        backend.getInput(prompt, details, defaultValue, ok, cancel);
    }

    public static void showLoginRefreshPrompt(MicrosoftAccount account) {
        backend.showLoginRefreshPrompt(account);
    }

    public interface EnvBackend {
        void showError(String message, String title);
        void showError(String message, Throwable t);
        void showInfo(String message, String title);
        default void showOkCancel(String message, String title, Runnable ok, Runnable cancel) {
            showOkCancel(message, title, ok, cancel, false);
        }
        void showOkCancel(String message, String title, Runnable ok, Runnable cancel, boolean defaultCancel);
        void getInput(String prompt, String details, String defaultValue, Consumer<String> ok, Runnable cancel) throws IOException;
        void showLoginRefreshPrompt(MicrosoftAccount account);
    }
}
