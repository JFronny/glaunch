package io.gitlab.jfronny.inceptum.launcher.system.mds.flow;

import io.gitlab.jfronny.commons.throwable.*;
import io.gitlab.jfronny.commons.tuple.Tuple;
import io.gitlab.jfronny.inceptum.launcher.system.mds.*;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

public class MdsPipeline {
    private final List<Tuple<ScanStage, ThrowingConsumer<Tuple<Path, Mod>, IOException>>> functions = new ArrayList<>();

    public void addTask(ScanStage stage, @Nullable ThrowingConsumer<MdsMod, IOException> task) {
        if (task == null) return;
        functions.add(Tuple.of(stage, tuple -> {
            if (tuple.right() instanceof MdsMod mmod) task.accept(mmod);
        }));
    }

    public void addFeedbackTask(@Nullable ThrowingBiConsumer<Path, Mod, IOException> task) {
        if (task == null) return;
        functions.add(Tuple.of(ScanStage.NONE, tuple -> {
            if (tuple.right() != null) task.accept(tuple.left(), tuple.right());
        }));
    }

    public void addTask(ScanStage stage, @Nullable ThrowingBiConsumer<Path, MdsMod, IOException> task) {
        if (task == null) return;
        functions.add(Tuple.of(stage, tuple -> {
            if (tuple.right() instanceof MdsMod mmod) task.accept(tuple.left(), mmod);
        }));
    }

    private void work(MdsThreadFactory factory, Tuple<Path, Mod> tuple, Queue<Tuple<ScanStage, ThrowingConsumer<Tuple<Path, Mod>, IOException>>> tasks, Consumer<Throwable> fail) {
        Thread we = Thread.currentThread();
        while (!tasks.isEmpty()) {
            Tuple<ScanStage, ThrowingConsumer<Tuple<Path, Mod>, IOException>> task = tasks.poll();
            factory.reprioritize(we, task.left());
            try {
                Thread.sleep(0);
            } catch (InterruptedException e) {
                we.interrupt();
            }
            try {
                task.right().accept(tuple);
            } catch (IOException e) {
                fail.accept(e);
                return;
            }
        }
    }

    public List<CompletableFuture<Void>> run(MdsThreadFactory factory, Set<Path> paths, ThrowingFunction<Path, Mod, IOException> seed) {
        return paths.stream().map(s -> {
            CompletableFuture<Void> finished = new CompletableFuture<>();
            Queue<Tuple<ScanStage, ThrowingConsumer<Tuple<Path, Mod>, IOException>>> taskQueue = new LinkedList<>(functions);
            taskQueue.add(Tuple.of(ScanStage.NONE, tuple -> finished.complete(null)));
            Consumer<Throwable> fail = finished::completeExceptionally;
            factory.newThread(MdsThreadFactory.prioritize(() -> {
                seed.andThen(mod -> {
                    work(factory, Tuple.of(s, mod), taskQueue, fail);
                }).addHandler(fail).accept(s);
            }, ScanStage.DISCOVER)).start();
            return finished;
        }).toList();
    }

    public List<CompletableFuture<Void>> run(MdsThreadFactory factory, Set<Tuple<Path, Mod>> mods) {
        return mods.stream().map(s -> {
            CompletableFuture<Void> finished = new CompletableFuture<>();
            Queue<Tuple<ScanStage, ThrowingConsumer<Tuple<Path, Mod>, IOException>>> taskQueue = new LinkedList<>(functions);
            taskQueue.add(Tuple.of(ScanStage.NONE, tuple -> finished.complete(null)));
            Consumer<Throwable> fail = finished::completeExceptionally;
            factory.newThread(MdsThreadFactory.prioritize(() -> {
                work(factory, s, taskQueue, fail);
            }, taskQueue.peek().left())).start();
            return finished;
        }).toList();
    }
}
