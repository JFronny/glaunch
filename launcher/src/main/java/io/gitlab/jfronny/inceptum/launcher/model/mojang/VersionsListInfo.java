package io.gitlab.jfronny.inceptum.launcher.model.mojang;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;

import java.util.Date;
import java.util.Objects;

@GSerializable
public class VersionsListInfo {
    public String id;
    public String type;
    public String url;
    public Date time;
    public Date releaseTime;
    public String sha1;
    public Integer complianceLevel;

    public void copyFrom(VersionsListInfo vli) {
        this.id = vli.id;
        this.type = vli.type;
        this.url = vli.url;
        this.time = vli.time == null ? null : new Date(vli.time.getTime());
        this.releaseTime = vli.releaseTime == null ? null : new Date(vli.releaseTime.getTime());
        this.sha1 = vli.sha1;
        this.complianceLevel = vli.complianceLevel;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof VersionsListInfo li && Objects.equals(id, li.id);
    }
}
