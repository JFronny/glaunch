package io.gitlab.jfronny.inceptum.launcher.system.mds.flow;

import io.gitlab.jfronny.commons.http.client.HttpClient;
import io.gitlab.jfronny.commons.throwable.ThrowingConsumer;
import io.gitlab.jfronny.inceptum.common.GsonPreset;
import io.gitlab.jfronny.inceptum.common.Utils;
import io.gitlab.jfronny.inceptum.launcher.model.fabric.FabricModJson;
import io.gitlab.jfronny.inceptum.launcher.model.fabric.GC_FabricModJson;
import io.gitlab.jfronny.inceptum.launcher.system.instance.ModPath;
import io.gitlab.jfronny.inceptum.launcher.system.mds.*;
import io.gitlab.jfronny.inceptum.launcher.system.source.DistributionDisabledException;
import io.gitlab.jfronny.inceptum.launcher.system.source.ModSource;

import java.io.IOException;
import java.nio.file.*;

public record MdsDownloadTask(ProtoInstance instance) implements ThrowingConsumer<MdsMod, IOException> {
    @Override
    public void accept(MdsMod mod) throws IOException {
        if (mod.getScanStage().contains(ScanStage.DOWNLOAD)) return;
        ModSource selectedSource = null;
        DistributionDisabledException distributionDisabledException = null;
        for (ModSource source : mod.getMetadata().sources().keySet()) {
            try {
                if (!Files.exists(source.getJarPath()) && HttpClient.wasOnline()) source.download();
                selectedSource = source;
            } catch (DistributionDisabledException de) {
                distributionDisabledException = de;
            }
        }
        Path imodPath = mod.getMetadataPath();
        Path jarPath = mod.getJarPath();
        boolean managed = false;
        if (selectedSource != null) {
            if (jarPath.startsWith(instance.modsDir()) && Files.exists(jarPath)) {
                if (Files.exists(selectedSource.getJarPath())) {
                    Files.delete(jarPath);
                    Path newImod = imodPath.getParent().resolve(selectedSource.getShortName() + ModPath.EXT_IMOD);
                    Files.move(imodPath, newImod);
                    imodPath = newImod;
                    jarPath = selectedSource.getJarPath();
                    managed = true;
                }
            } else {
                jarPath = selectedSource.getJarPath();
                managed = true;
            }
        } else if (!Files.exists(jarPath)) {
            IOException exception = new IOException("Mod has no jar and no sources");
            if (distributionDisabledException != null) exception.addSuppressed(distributionDisabledException);
            throw exception;
        }

        FabricModJson fmj;
        try (FileSystem fs = Utils.openZipFile(jarPath, false)) {
            Path fmjPath = fs.getPath("fabric.mod.json");
            if (Files.exists(fmjPath)) fmj = GC_FabricModJson.deserialize(fmjPath, GsonPreset.API);
            else fmj = null;
        }

        mod.markDownloaded(imodPath, jarPath, managed, fmj);
    }
}
