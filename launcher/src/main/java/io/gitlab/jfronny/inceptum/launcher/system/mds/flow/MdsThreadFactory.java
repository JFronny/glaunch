package io.gitlab.jfronny.inceptum.launcher.system.mds.flow;

import io.gitlab.jfronny.commons.flow.*;
import io.gitlab.jfronny.inceptum.launcher.system.mds.ScanStage;
import io.gitlab.jfronny.inceptum.launcher.util.VoidClaimPool;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Set;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class MdsThreadFactory implements ThreadFactory {
    protected static final ThreadPoolExecutor scheduler = DefaultSchedulers.createPriorityScheduler();

    private static final AtomicInteger poolNumber = new AtomicInteger(1);
    public final VoidClaimPool focusClaim = new VoidClaimPool(this::focus, this::defocus);
    private final AtomicInteger threadNumber = new AtomicInteger(1);
    private final String namePrefix;
    private final Set<OwnedThread> ownedRunnables = ConcurrentHashMap.newKeySet();

    public MdsThreadFactory(String name) {
        this.namePrefix = name + "-" + poolNumber.getAndIncrement() + "-";
    }

    public static Runnable prioritize(Runnable runnable, ScanStage stage) {
        return PrioritizedRunnable.of(runnable, toPriority(stage));
    }

    public static int toPriority(ScanStage stage) {
        return 5 - stage.ordinal();
    }

    @Override
    public Thread newThread(@NotNull Runnable runnable) {
        return newThread(runnable, null);
    }

    public Thread newThread(@NotNull Runnable runnable, @Nullable String name) {
        int priority = runnable instanceof PrioritizedRunnable pr ? pr.getPriority() : 0;
        OwnedThread ownedRunnable = new OwnedThread(runnable, new Thread[1], this);
        Thread t = ScheduledVirtualThreadBuilder.ofVirtual(scheduler)
                .name(this.namePrefix + this.threadNumber.getAndIncrement() + (name == null ? "": "-" + name))
                .unstarted(ownedRunnable);
        ownedRunnable.pfThread[0] = t;
        synchronized (focusClaim) {
            ownedRunnables.add(ownedRunnable);
            ScheduledVirtualThreadBuilder.setPriority(t, focusClaim.isEmpty() ? priority : priority + 5);
        }
        return t;
    }

    record OwnedThread(Runnable runnable, Thread[] pfThread, MdsThreadFactory pool) implements Runnable {
        @Override
        public void run() {
            try {
                runnable.run();
            } finally {
                pool.ownedRunnables.remove(this);
            }
        }
    }

    public static void reprioritize(Thread thread, int delta) {
        ScheduledVirtualThreadBuilder.setPriority(thread, ScheduledVirtualThreadBuilder.getPriority(thread) + delta);
    }

    public void reprioritize(Thread thread, ScanStage stage) {
        synchronized (focusClaim) {
            ScheduledVirtualThreadBuilder.setPriority(thread, focusClaim.isEmpty() ? toPriority(stage) : toPriority(stage) + 5);
        }
    }

    private void focus() {
        ownedRunnables.forEach(r -> reprioritize(r.pfThread[0], 5));
    }

    private void defocus() {
        ownedRunnables.forEach(r -> reprioritize(r.pfThread[0], -5));
    }
}
