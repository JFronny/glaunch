package io.gitlab.jfronny.inceptum.launcher.model.mojang;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;

import java.util.List;

@GSerializable
public record VersionsList(Latest latest, List<VersionsListInfo> versions) {
    @GSerializable
    public record Latest(String release, String snapshot) {
    }
}
