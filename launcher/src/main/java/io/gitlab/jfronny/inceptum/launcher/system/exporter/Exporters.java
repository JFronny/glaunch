package io.gitlab.jfronny.inceptum.launcher.system.exporter;

import java.util.List;

public class Exporters {
    public static final int STEP_COUNT = 4;
    public static final CurseForgeExporter CURSE_FORGE = new CurseForgeExporter();
    public static final ModrinthExporter MODRINTH = new ModrinthExporter();
    public static final MultiMCExporter MULTI_MC = new MultiMCExporter();
    public static final List<Exporter<?>> EXPORTERS = List.of(CURSE_FORGE, MODRINTH, MULTI_MC);
}
