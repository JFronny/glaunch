package io.gitlab.jfronny.inceptum.launcher.system.instance;

import io.gitlab.jfronny.inceptum.common.Utils;

import java.nio.file.Files;
import java.nio.file.Path;

public class ModPath {
    public static final String EXT_DISABLED = ".disabled";
    public static final String EXT_IMOD = ".imod";
    public static final String EXT_IMOD_DISABLED2 = EXT_DISABLED + EXT_IMOD;
    public static final String EXT_IMOD_DISABLED = EXT_IMOD + EXT_DISABLED;
    public static final String EXT_JAR = ".jar";
    public static final String EXT_JAR_DISABLED = EXT_JAR + EXT_DISABLED;

    public static boolean isImod(Path p) {
        return fn(p).endsWith(EXT_IMOD) || fn(p).endsWith(EXT_IMOD_DISABLED);
    }

    public static Path trimImod(Path p) {
        String fileName = fn(p);
        fileName = fileName.endsWith(EXT_IMOD_DISABLED)
                ? fileName.substring(0, fileName.length() - EXT_IMOD_DISABLED.length())
                : fileName.endsWith(EXT_IMOD_DISABLED2)
                ? fileName.substring(0, fileName.length() - EXT_IMOD_DISABLED2.length())
                : fileName.substring(0, fileName.length() - EXT_IMOD.length());
        return p.getParent().resolve(fileName);
    }

    public static boolean hasImod(Path mod) {
        Path parent = mod.getParent();
        if (parent == null) {
            Utils.LOGGER.info("The mod file " + mod + " doesn't have a parent. What is happening here?");
            return false;
        }
        return Files.exists(appendImod(mod));
    }

    public static Path appendImod(Path p) {
        return p.getParent().resolve(fn(p) + EXT_IMOD);
    }

    public static boolean isJar(Path p) {
        return fn(p).endsWith(EXT_JAR) || fn(p).endsWith(EXT_JAR_DISABLED);
    }

    public static boolean isEnabled(Path p) {
        return !fn(p).endsWith(EXT_DISABLED) && !fn(p).endsWith(EXT_IMOD_DISABLED2);
    }

    public static Path enable(Path p) {
        String fileName = fn(p);
        fileName = fileName.endsWith(EXT_IMOD_DISABLED2)
                ? fileName.substring(0, fileName.length() - EXT_IMOD_DISABLED2.length()) + EXT_IMOD
                : fileName.substring(0, fileName.length() - EXT_DISABLED.length());
        return p.getParent().resolve(fileName);
    }

    public static Path disable(Path p) {
        String fileName = fn(p);
        fileName = fileName.endsWith(EXT_IMOD)
                ? fileName.substring(0, fileName.length() - EXT_IMOD.length()) + EXT_IMOD_DISABLED2
                : fileName + EXT_DISABLED;
        return p.getParent().resolve(fileName);
    }

    public static Path toggle(Path p) {
        return isEnabled(p) ? disable(p) : enable(p);
    }

    private static String fn(Path p) {
        return p.getFileName().toString();
    }
}
