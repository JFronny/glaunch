plugins {
    inceptum.library
    inceptum.`gson-compile`
}

dependencies {
    api(projects.common)
    api(libs.commons.http.server) // required for launcher-gtk for some reason
    api(libs.commons.flow)
    api(libs.commons.flow.backend.unsafe)
}
