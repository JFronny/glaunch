package io.gitlab.jfronny.inceptum.launchwrapper;

import java.io.File;
import java.lang.invoke.*;
import java.lang.reflect.Method;
import java.nio.file.*;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) throws Throwable {
        if (args.length == 0) throw new IllegalArgumentException("Missing class argument");
        System.out.println("Starting Inceptum launchwrapper");

        String forceloadNatives = System.getProperty("inceptum.forceloadNatives");
        if (forceloadNatives != null) {
            System.setProperty("java.library.path", System.getProperty("java.library.path", "") + File.pathSeparator + forceloadNatives);
            Path p = Paths.get(forceloadNatives);
            if (Files.exists(p)) {
                try (Stream<Path> paths = Files.list(p)) {
                    paths.forEach(Main::load);
                }
            } else {
                throw new IllegalArgumentException("Could not find forceloadNatives path");
            }
        }

        String[] newArgs = new String[args.length - 1];
        System.arraycopy(args, 1, newArgs, 0, args.length - 1);

        Class<?> mainClass = Class.forName(args[0]);
        Method mainMethod = mainClass.getMethod("main", String[].class);
        MethodHandles.Lookup lookup = MethodHandles.lookup();
        @SuppressWarnings("ConfusingArgumentToVarargsMethod") Runnable main = (Runnable) LambdaMetafactory.metafactory(
                lookup,
                "run",
                MethodType.methodType(Runnable.class, String[].class),
                MethodType.methodType(Void.TYPE),
                lookup.unreflect(mainMethod),
                MethodType.methodType(Void.TYPE)
        ).getTarget().invokeExact(newArgs);

        Thread th = new Thread(main);
        th.setName("Minecraft");
        th.start();
    }

    private static void load(Path path) {
        System.load(path.toAbsolutePath().normalize().toString());
    }
}
