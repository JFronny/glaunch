rootProject.name = "Inceptum"

include("common")
include("wrapper")
include("launcher")
include("launcher-cli")
include("launcher-imgui")
include("launcher-dist")
include("launchwrapper")
include("launcher-gtk")

enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")