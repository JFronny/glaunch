plugins {
    inceptum.application
}

application {
    mainClass.set("io.gitlab.jfronny.inceptum.cli.CliMain")
}

dependencies {
    implementation(projects.launcher)
}
