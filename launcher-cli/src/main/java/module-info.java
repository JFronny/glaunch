module io.gitlab.jfronny.inceptum.launcher.cli {
    exports io.gitlab.jfronny.inceptum.cli;

    requires transitive io.gitlab.jfronny.inceptum.launcher;
    requires static org.jetbrains.annotations;
}