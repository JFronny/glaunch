package io.gitlab.jfronny.inceptum.cli;

import io.gitlab.jfronny.inceptum.common.Utils;
import io.gitlab.jfronny.inceptum.launcher.LauncherEnv;
import io.gitlab.jfronny.inceptum.launcher.api.account.MicrosoftAccount;

import java.io.IOException;
import java.util.Scanner;
import java.util.function.Consumer;

public class CliEnvBackend implements LauncherEnv.EnvBackend {
    @Override
    public void showError(String message, String title) {
        Utils.LOGGER.error(message);
    }

    @Override
    public void showError(String message, Throwable t) {
        Utils.LOGGER.error(message, t);
    }

    @Override
    public void showInfo(String message, String title) {
        Utils.LOGGER.info(message);
    }

    @Override
    public void showOkCancel(String message, String title, Runnable ok, Runnable cancel, boolean defaultCancel) {
        Utils.LOGGER.info(message);
        if (!defaultCancel) ok.run();
    }

    @Override
    public void getInput(String prompt, String details, String defaultValue, Consumer<String> ok, Runnable cancel) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println(prompt);
        System.out.print("> ");
        ok.accept(scanner.nextLine());
    }

    @Override
    public void showLoginRefreshPrompt(MicrosoftAccount account) {
        Utils.LOGGER.error("Could not launch using the selected account as its login has expired. Please log in again through the UI!");
    }
}
