package io.gitlab.jfronny.inceptum.cli.commands;

import io.gitlab.jfronny.inceptum.cli.*;
import io.gitlab.jfronny.inceptum.launcher.system.exporter.Exporters;
import io.gitlab.jfronny.inceptum.launcher.system.instance.Instance;
import io.gitlab.jfronny.inceptum.launcher.util.ProcessState;

import java.nio.file.Paths;
import java.util.List;

public class ExportCommand extends BaseInstanceCommand {
    public ExportCommand() {
        this(List.of("export"), List.of(
                new ExportCommand(List.of("curseforge", "cf"), List.of()),
                new ModrinthExportCommand(List.of("modrinth", "mr"), List.of()),
                new MultiMCExportCommand(List.of("multimc", "mmc"), List.of())
        ));
    }

    private ExportCommand(List<String> aliases, List<Command> subCommands) {
        super("Export a CurseForge instance", "<export path> <version>", aliases, subCommands);
    }

    @Override
    protected void invoke(CommandArgs args, Instance instance) throws Exception {
        if (args.length == 0) throw new IllegalAccessException("You must specify a target path");
        if (args.length > 2) throw new IllegalAccessException("Too many arguments");
        if (args.length > 1) instance.meta().instanceVersion = args.get(1);
        Exporters.CURSE_FORGE.generate(new ProcessState(), instance, Paths.get(args.get(0)));
    }

    private static class MultiMCExportCommand extends BaseInstanceCommand {
        public MultiMCExportCommand(List<String> aliases, List<Command> subCommands) {
            super("Export a MultiMC instance", "<export path>", aliases, subCommands);
        }

        @Override
        protected void invoke(CommandArgs args, Instance instance) throws Exception {
            if (args.length == 0) throw new IllegalAccessException("You must specify a target path");
            if (args.length != 1) throw new IllegalAccessException("Too many arguments");
            Exporters.MULTI_MC.generate(new ProcessState(), instance, Paths.get(args.get(0)));
        }
    }

    private static class ModrinthExportCommand extends BaseInstanceCommand {
        public ModrinthExportCommand(List<String> aliases, List<Command> subCommands) {
            super("Export a Modrinth instance", "<export path> <version>", aliases, subCommands);
        }

        @Override
        protected void invoke(CommandArgs args, Instance instance) throws Exception {
            if (args.length == 0) throw new IllegalAccessException("You must specify a target path");
            if (args.length > 2) throw new IllegalAccessException("Too many arguments");
            if (args.length > 1) instance.meta().instanceVersion = args.get(1);
            Exporters.MODRINTH.generate(new ProcessState(), instance, Paths.get(args.get(0)));
        }
    }
}
