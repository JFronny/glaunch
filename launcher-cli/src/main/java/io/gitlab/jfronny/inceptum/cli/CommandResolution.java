package io.gitlab.jfronny.inceptum.cli;

import java.util.List;

public record CommandResolution(Command command, CommandArgs args, List<String> resolvePath) {
    public void invoke() throws Exception {
        command.invoke(args);
    }
}
