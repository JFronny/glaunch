plugins {
    inceptum.application
}

application {
    mainClass.set("io.gitlab.jfronny.inceptum.imgui.GuiMain")
}

dependencies {
    val flavor: String by rootProject.extra

    implementation(projects.launcher)

    fun forEachPlatform(action: (String) -> Unit) {
        (if (flavor == "fat") listOf("windows", "linux", "macos") else listOf(flavor)).forEach(action)
    }

    implementation(libs.bundles.lwjgl)
    libs.bundles.lwjgl.natives.get().forEach {
        forEachPlatform { suffix -> implementation(variantOf(provider { it }) { classifier("natives-$suffix") }) }
    }

    implementation(libs.imgui)
    implementation(libs.imgui.lwjgl)
    if (flavor == "windows" || flavor == "fat") implementation(libs.imgui.natives.windows)
    if (flavor == "linux" || flavor == "fat") implementation(libs.imgui.natives.linux)
    if (flavor == "macos" || flavor == "fat") implementation(libs.imgui.natives.macos)
}
