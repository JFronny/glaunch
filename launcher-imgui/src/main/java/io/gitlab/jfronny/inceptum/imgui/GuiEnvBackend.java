package io.gitlab.jfronny.inceptum.imgui;

import io.gitlab.jfronny.commons.StringFormatter;
import io.gitlab.jfronny.inceptum.common.Utils;
import io.gitlab.jfronny.inceptum.imgui.window.MicrosoftLoginWindow;
import io.gitlab.jfronny.inceptum.imgui.window.dialog.AlertWindow;
import io.gitlab.jfronny.inceptum.imgui.window.dialog.TextBoxWindow;
import io.gitlab.jfronny.inceptum.launcher.LauncherEnv;
import io.gitlab.jfronny.inceptum.launcher.api.account.MicrosoftAccount;

import java.io.IOException;
import java.util.function.Consumer;

public class GuiEnvBackend implements LauncherEnv.EnvBackend {
    @Override
    public void showError(String message, String title) {
        Utils.LOGGER.error(message);
        GuiMain.WINDOWS.add(new AlertWindow(title, message));
    }

    @Override
    public void showError(String message, Throwable t) {
        Utils.LOGGER.error(message, t);
        GuiMain.WINDOWS.add(new AlertWindow(message, StringFormatter.toString(t)));
    }

    @Override
    public void showInfo(String message, String title) {
        Utils.LOGGER.info(message);
        GuiMain.WINDOWS.add(new AlertWindow(title, message));
    }

    @Override
    public void showOkCancel(String message, String title, Runnable ok, Runnable cancel, boolean defaultCancel) {
        Utils.LOGGER.info(message);
        GuiMain.WINDOWS.add(new AlertWindow(title, message, ok, cancel));
    }

    @Override
    public void getInput(String prompt, String details, String defaultValue, Consumer<String> ok, Runnable cancel) throws IOException {
        GuiMain.WINDOWS.add(new TextBoxWindow(prompt, details, defaultValue, ok, cancel));
    }

    @Override
    public void showLoginRefreshPrompt(MicrosoftAccount account) {
        GuiMain.open(new MicrosoftLoginWindow(account));
    }
}
