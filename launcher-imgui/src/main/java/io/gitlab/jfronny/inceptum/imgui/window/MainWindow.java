package io.gitlab.jfronny.inceptum.imgui.window;

import imgui.ImGui;
import imgui.flag.ImGuiWindowFlags;
import imgui.type.ImBoolean;
import imgui.type.ImInt;
import io.gitlab.jfronny.commons.io.JFiles;
import io.gitlab.jfronny.inceptum.common.*;
import io.gitlab.jfronny.inceptum.imgui.GuiMain;
import io.gitlab.jfronny.inceptum.imgui.control.InstanceView;
import io.gitlab.jfronny.inceptum.launcher.LauncherEnv;
import io.gitlab.jfronny.inceptum.launcher.api.account.*;

import java.io.IOException;
import java.util.List;

public class MainWindow extends Window {
    private final ImBoolean darkTheme = new ImBoolean(InceptumConfig.darkTheme);
    private final ImBoolean debugTools = new ImBoolean(false);
    private final ImInt accountIndex = new ImInt(AccountManager.getSelectedIndex());

    public MainWindow() {
        super("Inceptum");
    }

    @Override
    public int getFlags() {
        return ImGuiWindowFlags.MenuBar | ImGuiWindowFlags.AlwaysAutoResize;
    }

    @Override
    public void draw() {
        ImGui.beginMenuBar();
        if (ImGui.beginMenu("File")) {
            if (ImGui.menuItem("New Instance")) GuiMain.open(new NewInstanceWindow());
            if (ImGui.menuItem("Re-download resources")) {
                try {
                    JFiles.clearDirectory(MetaHolder.ASSETS_DIR);
                    JFiles.clearDirectory(MetaHolder.LIBRARIES_DIR, path -> !path.startsWith(MetaHolder.LIBRARIES_DIR.resolve("io/gitlab/jfronny")));
                    JFiles.clearDirectory(MetaHolder.NATIVES_DIR, path -> !path.endsWith("forceload"));
                    JFiles.clearDirectory(MetaHolder.CACHE_DIR);
                    GuiUtil.reload();
                } catch (IOException e) {
                    LauncherEnv.showError("Could not execute refresh task", e);
                }
            }
            if (ImGui.menuItem("Exit Inceptum")) GuiMain.exit();
            ImGui.endMenu();
        }
        if (ImGui.beginMenu("Account")) {
            if (ImGui.menuItem("New")) GuiMain.open(new MicrosoftLoginWindow());
            AuthInfo selected = AccountManager.getSelectedAccount();
            List<MicrosoftAccount> accounts = AccountManager.getAccounts();
            int accountsSize = accounts.size();
            for (int i = 0; i < accountsSize; i++) {
                MicrosoftAccount account = accounts.get(i);
                if (selected.equals(account)) accountIndex.set(i);
                if (ImGui.radioButton(account.minecraftUsername, accountIndex, i)) {
                    AccountManager.switchAccount(account);
                }
                ImGui.sameLine();
                if (ImGui.button("X")) AccountManager.removeAccount(account);
            }
            if (ImGui.radioButton("None", accountIndex, accountsSize)) {
                AccountManager.switchAccount(null);
            }
            ImGui.endMenu();
        }
        if (ImGui.beginMenu("Settings")) {
            if (ImGui.checkbox("Dark Theme", darkTheme)) {
                InceptumConfig.darkTheme = darkTheme.get();
                InceptumConfig.saveConfig();
                GuiMain.applyTheme();
            }
            ImGui.endMenu();
        }
        if (ImGui.beginMenu("Help")) {
            if (ImGui.menuItem("About")) GuiMain.open(new AboutWindow());
            if (ImGui.menuItem("Log")) GuiMain.open(new LogWindow(GuiMain.MEMLOG));
            ImGui.checkbox("Debug Tools", debugTools);
            ImGui.endMenu();
        }
        ImGui.endMenuBar();

        if (debugTools.get()) {
            ImGui.showDemoWindow();
        }

        try {
            InstanceView.draw();
        } catch (IOException e) {
            Utils.LOGGER.error("Could not show instance list view");
        }
    }
}
