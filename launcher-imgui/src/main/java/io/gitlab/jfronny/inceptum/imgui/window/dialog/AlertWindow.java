package io.gitlab.jfronny.inceptum.imgui.window.dialog;

import imgui.ImGui;
import imgui.flag.ImGuiWindowFlags;
import io.gitlab.jfronny.inceptum.imgui.window.Window;

public class AlertWindow extends Window {
    private final String message;
    private final Runnable onOk;
    private final Runnable onCancel;
    private boolean success = false;

    public AlertWindow(String title, String message) {
        this(title, message, null, null);
    }

    public AlertWindow(String title, String message, Runnable onOk, Runnable onCancel) {
        super(title);
        this.message = message;
        this.onOk = onOk;
        this.onCancel = onCancel;
    }

    @Override
    public void draw() {
        ImGui.text(message);
        if (ImGui.button("OK")) {
            success = true;
            close();
        }
        if (onOk != null || onCancel != null) {
            ImGui.sameLine();
            if (ImGui.button("Cancel")) {
                success = false;
                close();
            }
        }
    }

    @Override
    public int getFlags() {
        return super.getFlags() | ImGuiWindowFlags.NoSavedSettings;
    }

    @Override
    public void close() {
        super.close();
        if (success) {
            if (onOk != null) onOk.run();
        } else {
            if (onCancel != null) onCancel.run();
        }
    }
}
