package io.gitlab.jfronny.inceptum.imgui.control;

import imgui.ImGui;
import imgui.flag.ImGuiTableFlags;
import io.gitlab.jfronny.inceptum.common.Utils;
import io.gitlab.jfronny.inceptum.imgui.GuiMain;
import io.gitlab.jfronny.inceptum.imgui.window.edit.InstanceEditWindow;
import io.gitlab.jfronny.inceptum.launcher.system.instance.Instance;
import io.gitlab.jfronny.inceptum.launcher.system.instance.InstanceList;
import io.gitlab.jfronny.inceptum.launcher.system.launch.InstanceLauncher;
import io.gitlab.jfronny.inceptum.launcher.system.setup.Steps;

import java.io.IOException;
import java.nio.file.Files;

public class InstanceView {
    public static void draw() throws IOException {
        if (InstanceList.isEmpty()) {
            ImGui.text("You have not yet created an instance");
            ImGui.text("Use File->New Instance to do so");
            return;
        }
        if (ImGui.beginTable("Instances", 2, ImGuiTableFlags.SizingFixedFit | ImGuiTableFlags.Borders)) {
            for (Instance instance : InstanceList.ordered()) {
                if (instance.isSetupLocked()) {
                    ImGui.tableNextColumn();
                    ImGui.text("Setting up");
                    ImGui.tableNextColumn();
                    ImGui.text("This instance is currently being set up");
                    continue;
                }
                if (!Files.exists(instance.path().resolve(Instance.CONFIG_NAME))) {
                    Utils.LOGGER.error("Invalid instance (doesn't contain " + Instance.CONFIG_NAME + "): " + instance);
                    continue;
                }
                ImGui.tableNextColumn();
                boolean runDisabled = instance.isRunningLocked();
                if (runDisabled) ImGui.beginDisabled();
                if (ImGui.button(instance.toString())) {
                    try {
                        Steps.reDownload(instance, Steps.createProcessState());
                    } catch (IOException e) {
                        Utils.LOGGER.error("Could not redownload instance, trying to start anyways", e);
                    }
                    InstanceLauncher.launchClient(instance);
                }
                if (runDisabled) ImGui.endDisabled();
                ImGui.tableNextColumn();
                if (ImGui.button("Edit##" + instance.id())) {
                    try {
                        GuiMain.open(new InstanceEditWindow(instance));
                    } catch (IOException e) {
                        Utils.LOGGER.error("Could not open instance edit window", e);
                    }
                }
            }
            ImGui.endTable();
        }
    }
}
