package io.gitlab.jfronny.inceptum.imgui.window;

import imgui.ImGui;

public class LogWindow extends Window {
    private final Iterable<String> source;

    public LogWindow(Iterable<String> source) {
        super("Log");
        this.source = source;
    }

    @Override
    public void draw() {
        source.forEach(ImGui::textUnformatted);
        if (ImGui.getScrollY() >= ImGui.getScrollMaxY())
            ImGui.setScrollHereY(1.0f);
    }

    @Override
    public int getFlags() {
        return 0;
    }
}
