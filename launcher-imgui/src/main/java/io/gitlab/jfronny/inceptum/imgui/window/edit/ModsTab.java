package io.gitlab.jfronny.inceptum.imgui.window.edit;

import imgui.ImGui;
import imgui.type.ImBoolean;
import io.gitlab.jfronny.inceptum.common.Utils;
import io.gitlab.jfronny.inceptum.imgui.GuiMain;
import io.gitlab.jfronny.inceptum.imgui.control.Tab;
import io.gitlab.jfronny.inceptum.imgui.window.AddModWindow;
import io.gitlab.jfronny.inceptum.launcher.LauncherEnv;
import io.gitlab.jfronny.inceptum.launcher.system.mds.Mod;
import io.gitlab.jfronny.inceptum.launcher.system.instance.ModPath;
import io.gitlab.jfronny.inceptum.launcher.system.source.ModSource;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class ModsTab extends Tab {
    private final InstanceEditWindow window;
    private final ImBoolean filterUpdates = new ImBoolean();
    private Path selected = null;

    public ModsTab(InstanceEditWindow window) {
        super("Mods");
        this.window = window;
    }

    @Override
    protected void renderInner() {
        window.lastTabWasMods = true;
        if (!Files.exists(window.instance.getModsDir())) {
            try {
                Files.createDirectories(window.instance.getModsDir());
            } catch (IOException e) {
                Utils.LOGGER.error("Could not create mods directory which was missing from this modded instance", e);
            }
        }
        ImGui.beginChild("mods select", 200, 0);
        if (ImGui.button("Add")) {
            GuiMain.WINDOWS.add(new AddModWindow(window.instance));
        }
        ImGui.sameLine();
        if (Files.exists(window.instance.getModsDir()) && ImGui.button("Show")) {
            Utils.openFile(window.instance.getModsDir().toFile());
        }
        ImGui.sameLine();
        if (Files.exists(window.instance.getConfigDir()) && ImGui.button("Configs")) {
            Utils.openFile(window.instance.getConfigDir().toFile());
        }
        try {
            Set<Mod> modSet = window.instance.mds().getMods();
            boolean updatesFound = false;
            float scannedPercentage = 0;
            boolean hasUnScanned = false;
            for (Mod mod : modSet) {
                if (window.instance.mds().hasScanned(mod)) scannedPercentage++;
                else hasUnScanned = true;
                for (Optional<ModSource> value : mod.getMetadata().sources().values()) {
                    if (value.isPresent()) {
                        updatesFound = true;
                        break;
                    }
                    if (updatesFound)
                        break;
                }
            }
            scannedPercentage /= modSet.size();
            if (hasUnScanned) ImGui.progressBar(scannedPercentage);
            if (updatesFound)
                ImGui.checkbox("Updatable", filterUpdates);
            else
                filterUpdates.set(false);
            ImGui.separator();
            for (Mod mod : modSet) {
                updatesFound = false;
                for (Optional<ModSource> value : mod.getMetadata().sources().values()) {
                    updatesFound |= value.isPresent();
                }
                if (filterUpdates.get() && !updatesFound) continue;
                if (ImGui.checkbox("##" + mod.getName(), mod.isEnabled())) {
                    Path newSel = ModPath.toggle(mod.getMetadataPath());
                    try {
                        Files.move(mod.getMetadataPath(), newSel);
                        if (mod.getMetadataPath().equals(selected)) selected = newSel;
                    } catch (IOException e) {
                        LauncherEnv.showError("Could not change disabled state", e);
                    }
                }
                ImGui.sameLine();
                if (ImGui.button(mod.getName())) selected = mod.getMetadataPath();
            }
        } catch (IOException e) {
            Utils.LOGGER.error("Could not show mod list", e);
        }
        ImGui.endChild();
        ImGui.sameLine();
        ImGui.beginGroup();
        if (selected == null) {
            ImGui.text("Select a mod to view settings");
        } else if (window.instance.mds().hasScanned(selected)) {
            Mod md = window.instance.mds().get(selected);
            ImGui.text(md.getName());
            ImGui.separator();
            for (String s : md.getDescription()) {
                ImGui.text(s);
            }
            ImGui.separator();
            Map<ModSource, Optional<ModSource>> sources = md.getMetadata().sources();
            ImGui.text("Sources:");
            if (sources.isEmpty())
                ImGui.bulletText("Local Drive");
            else {
                for (var source : sources.entrySet()) {
                    ImGui.bulletText(source.getKey().getName());
                    source.getValue().ifPresent(update -> {
                        ImGui.sameLine();
                        if (ImGui.button("Update to " + update.getVersion())) {
                            try {
                                selected = md.update(update);
                            } catch (IOException e) {
                                LauncherEnv.showError("Update failed", e);
                            }
                        }
                    });
                }
            }
            if (ImGui.button("Delete")) {
                if (!md.getMetadata().dependents().isEmpty())
                    LauncherEnv.showError("This mod still has the following dependent mods installed: " + String.join(", ", md.getMetadata().dependents()), "Dependents present");
                else {
                    try {
                        md.delete();
                    } catch (IOException e) {
                        LauncherEnv.showError("Couldn't delete the file", e);
                    }
                    selected = null;
                }
            }
        } else {
            ImGui.text("This mod has not yet been scanned, please be patient");
        }
        ImGui.endGroup();
    }

    @Override
    protected boolean isVisible() {
        return window.instance.isFabric();
    }
}
