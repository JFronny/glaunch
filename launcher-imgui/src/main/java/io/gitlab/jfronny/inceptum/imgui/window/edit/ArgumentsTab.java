package io.gitlab.jfronny.inceptum.imgui.window.edit;

import imgui.ImGui;
import imgui.type.ImString;
import io.gitlab.jfronny.inceptum.common.Utils;
import io.gitlab.jfronny.inceptum.imgui.control.Tab;
import io.gitlab.jfronny.inceptum.imgui.window.GuiUtil;
import io.gitlab.jfronny.inceptum.launcher.model.inceptum.InstanceMeta;

import java.util.List;

public class ArgumentsTab extends Tab {
    private final InstanceEditWindow window;
    private final ImString jvm = new ImString(GuiUtil.INPUT_FIELD_LENGTH);
    private final ImString client = new ImString(GuiUtil.INPUT_FIELD_LENGTH);
    private final ImString server = new ImString(GuiUtil.INPUT_FIELD_LENGTH);

    public ArgumentsTab(InstanceEditWindow window) {
        super("Arguments");
        this.window = window;
        InstanceMeta meta = window.instance.meta();
        meta.checkArguments();
        jvm.set(String.join("\n", meta.arguments.jvm()));
        client.set(String.join("\n", meta.arguments.client()));
        server.set(String.join("\n", meta.arguments.server()));
    }

    @Override
    protected void renderInner() {
        InstanceMeta meta = window.instance.meta();
        if (ImGui.inputTextMultiline("JVM", jvm)) {
            meta.arguments = meta.arguments.withJvm(List.of(Utils.NEW_LINE.split(jvm.get())));
            window.instance.writeMeta();
        }
        if (ImGui.inputTextMultiline("Client", client)) {
            meta.arguments = meta.arguments.withClient(List.of(Utils.NEW_LINE.split(client.get())));
            window.instance.writeMeta();
        }
        if (ImGui.inputTextMultiline("Server", server)) {
            meta.arguments = meta.arguments.withServer(List.of(Utils.NEW_LINE.split(server.get())));
            window.instance.writeMeta();
        }
    }
}
