module io.gitlab.jfronny.inceptum.launcher.imgui {
    exports io.gitlab.jfronny.inceptum.imgui;

    requires transitive io.gitlab.jfronny.inceptum.launcher;
    requires static org.jetbrains.annotations;
    requires imgui.binding;
    requires org.lwjgl;
    requires org.lwjgl.glfw;
    requires org.lwjgl.opengl;
    requires org.lwjgl.tinyfd;
    requires imgui.lwjgl3;
}